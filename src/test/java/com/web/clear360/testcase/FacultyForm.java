package com.web.clear360.testcase;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.web.clear360.factory.LoginFactory;
import com.web.clear360.factory.StaffFactory;
import com.web.clear360.factory.UploadCsvFactory;
import com.web.clear360.utility.BaseclassWeb;
import com.web.clear360.utility.CommanMethod;

	public class FacultyForm extends BaseclassWeb {

		@Test
		public void FacultyForm() throws InterruptedException, IOException {

			LoginFactory Login = PageFactory.initElements(driver, LoginFactory.class);
			UploadCsvFactory Upload = PageFactory.initElements(driver, UploadCsvFactory.class);
			StaffFactory staff = PageFactory.initElements(driver, StaffFactory.class);

			Thread.sleep(1000);

			
			Login.getInput_UserName().sendKeys("mohammed.aslam+oo@aurigait.com");
			Login.getInput_Password().sendKeys("Aslam123*");
			Login.getButton_Login().click();
			driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
			Upload.getClick_Menu().click();
			
			  
			 Upload.getTab_ManageUsers().click();
			// driver.findElement(By.xpath("//span[contains(text(),'Faculty')]")).click();
			Upload.getTab_Faculty().click();
			staff.getBtn_NewFaculty().click();
			
			  staff.getFirstName().sendKeys("Faculty");
			  staff.getLastName().sendKeys("Positivetest");
			  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			  staff.getMobile().sendKeys("+919585603010");
			  
			  Select Grade = new Select(driver.findElement(By.id("departments")));
			  Grade.selectByVisibleText("Maths");
			  Select School = new Select(driver.findElement(By.id("locations")));
			  School.selectByVisibleText("Ram Nagar");
			  staff.getUniqueId().sendKeys("Facu");
			  staff.getBtn_Submit().click();
			
			 /* driver.findElement(By.xpath("//button[contains(text(),'Create New Faculty')]")).click();
			 driver.findElement(By.id("firstName")).sendKeys("urmila");
			  driver.findElement(By.id("lastName")).sendKeys("Staff");
			  
			 driver.findElement(By.id("mobile")).sendKeys("+919485623101");
			 
			 
			  Select Grade = new Select(driver.findElement(By.id("departments")));
			  Grade.selectByVisibleText("Maths");
			  Select School = new Select(driver.findElement(By.id("locations")));
			  School.selectByVisibleText("Ram Nagar");
			  driver.findElement(By.id("uniqueId")).sendKeys("urmis");
			  driver.findElement(By.xpath("//button[contains(text(),'Submit')]")).click();*/
		}
		@Test(priority=2)
		public void StaffMobileReq() throws InterruptedException, IOException {
			StaffFactory staff = PageFactory.initElements(driver, StaffFactory.class);
			staff.getBtn_NewFaculty().click();
			 staff.getFirstName().sendKeys("faculty");
			  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			  staff.getLastName().sendKeys("Mobile");
			  staff.getUniqueId().sendKeys("Bb");
			  staff.getBtn_Submit().click();
			  CommanMethod.checkValid(driver);
			  
		}
		@Test(priority=3)
		public void StaffLoc() throws InterruptedException, IOException {
			StaffFactory staff = PageFactory.initElements(driver, StaffFactory.class);
			 driver.findElement(By.xpath("//a[contains(text(),'Back')]")).click();
			 staff.getBtn_NewFaculty().click();
			 staff.getFirstName().sendKeys("Faculty");
			  staff.getLastName().sendKeys("Loc");
			  staff.getMobile().sendKeys("+919485603301");
			  staff.getUniqueId().sendKeys("Ac");
			  staff.getBtn_Submit().click();
			  CommanMethod.checkValid(driver);
			  
		}
		@Test(priority=4)
		public void StaffDepart() throws InterruptedException, IOException {
			StaffFactory staff = PageFactory.initElements(driver, StaffFactory.class);
			 driver.findElement(By.xpath("//a[contains(text(),'Back')]")).click();
			 staff.getBtn_NewFaculty().click();
			 staff.getFirstName().sendKeys("Staff");
			  staff.getLastName().sendKeys("Depart");
			  staff.getMobile().sendKeys("+919485603302");
			  Select School = new Select(driver.findElement(By.id("locations")));
			  School.selectByVisibleText("Ram Nagar");
			  staff.getUniqueId().sendKeys("Bd");
			  staff.getBtn_Submit().click();
			  CommanMethod.checkValid(driver);
}
		@Test(priority=5)
		public void StaffUniqueIdalredyused() throws InterruptedException, IOException {
			StaffFactory staff = PageFactory.initElements(driver, StaffFactory.class);
			 driver.findElement(By.xpath("//a[contains(text(),'Back')]")).click();
			 staff.getBtn_NewFaculty().click();
			 staff.getFirstName().sendKeys("Staff");
			  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			  staff.getLastName().sendKeys("UniqueID");
			  staff.getMobile().sendKeys("+919485603303");
			  staff.getUniqueId().sendKeys("Facu");
			  staff.getBtn_Submit().click();
			  CommanMethod.checkValid(driver);
}
		@Test(priority=6)
		public void MobileNumberIdalredyused() throws InterruptedException, IOException {
			StaffFactory staff = PageFactory.initElements(driver, StaffFactory.class);
			 driver.findElement(By.xpath("//a[contains(text(),'Back')]")).click();
			 staff.getBtn_NewFaculty().click();
			 staff.getFirstName().sendKeys("Staff");
			  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			  staff.getLastName().sendKeys("MobileAlready");
			  staff.getMobile().sendKeys("+919585603010");
			  staff.getUniqueId().sendKeys("Fgd");
			  staff.getBtn_Submit().click();
			  CommanMethod.checkValid(driver);
}
}
