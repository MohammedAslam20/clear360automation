package com.web.clear360.testcase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.web.clear360.factory.CommonFactory;
import com.web.clear360.factory.LoginFactory;
import com.web.clear360.factory.SuperAdminFactory;
import com.web.clear360.utility.BaseclassWeb;
import com.web.clear360.utility.CommanMethod;
import com.web.clear360.utility.SuperAdminCommon;

public class CreateOrgNegative extends BaseclassWeb {

	CommanMethod commonMethod = new CommanMethod();
	SuperAdminCommon superadmincommon = new SuperAdminCommon();
	public String loginEmail = "mangesh.darji23@aurigait.com";
	public String password = "Mangesh@123";
	WebElement OrgName, OrgContact, Orgmobile;

	@Test(priority = 1)
	public void invalidOrgName() throws InterruptedException {
		LoginFactory Login = PageFactory.initElements(driver, LoginFactory.class);
		CommonFactory common = PageFactory.initElements(driver, CommonFactory.class);
		SuperAdminFactory superadmin = PageFactory.initElements(driver, SuperAdminFactory.class);

		Thread.sleep(2000);
		commonMethod.credentialSuperAdmin(Login);
		;
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//body/div[1]/div[1]/div[1]/div[3]/form[1]/div[1]/div[1]/div[1]/div[1]/input[1]")));

		// go to manage org page_Admin
		commonMethod.goto_OrgPage(driver, common);
		Thread.sleep(2000);

		superadmin.getButton_CreateOrg().click();

		// select values
		OrgName = superadmin.getInput_OrgName();
		OrgName.sendKeys("23");
		Select orgType = new Select(superadmin.getInput_OrgType());
		orgType.selectByVisibleText("ORGANIZATION");
		OrgContact = superadmin.getInput_ContactName();
		OrgContact.sendKeys("MD14OCT");
		superadmin.getInput_ContactEmail().sendKeys("mangesh.darji@yusata.com");
		superadmin.getInput_Phone().sendKeys("+919983413914");
		superadmin.getInput_LoginEmail().sendKeys(loginEmail);
		superadmin.getInput_Password().sendKeys(password);
		superadmin.getButton_Submit().click();
		Thread.sleep(2000);
		String actualMsg = superadmin.getMsg_InvalidOrgName().getText();
		superadmincommon.invalidOrgName(actualMsg);
		OrgName.clear();
		OrgContact.clear();
	}
	@Test(priority = 2)
	public void invalidContactName() throws InterruptedException {
		SuperAdminFactory superadmin = PageFactory.initElements(driver, SuperAdminFactory.class);

		// select values
		superadmin.getInput_OrgName().sendKeys("org1");
		OrgContact = superadmin.getInput_ContactName();
		OrgContact.sendKeys("123");
		superadmin.getInput_Password().sendKeys(password);
		superadmin.getButton_Submit().click();
		Thread.sleep(2000);
		String actualMsg = superadmin.getMsg_InvalidContactName().getText();
		superadmincommon.invalidContactName(actualMsg);
		OrgContact.clear();
	}
	@Test(priority = 3)
	public void invalidMobile() throws InterruptedException {
		SuperAdminFactory superadmin = PageFactory.initElements(driver, SuperAdminFactory.class);
		
		Orgmobile = superadmin.getInput_Phone();
		Orgmobile.clear();

		// select values
		superadmin.getInput_OrgName().sendKeys("org1");
		OrgContact = superadmin.getInput_ContactName();
		OrgContact.sendKeys("mangesh");
		OrgContact.sendKeys("+91");
		superadmin.getInput_Password().sendKeys(password);
		superadmin.getButton_Submit().click();
		Thread.sleep(2000);
		String actualMsg = superadmin.getMsg_InvalidMobile().getText();
		superadmincommon.invalidMobile(actualMsg);
		OrgContact.clear();
	}

	/*
	 * @AfterTest public void terminateBrowser() { driver.close(); }
	 */

}
