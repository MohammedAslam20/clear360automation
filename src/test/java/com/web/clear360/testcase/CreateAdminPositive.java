package com.web.clear360.testcase;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import com.web.clear360.factory.AdminFactory;
import com.web.clear360.factory.CommonFactory;
import com.web.clear360.factory.LoginFactory;
import com.web.clear360.utility.BaseclassWeb;
import com.web.clear360.utility.CheckinCommon;
import com.web.clear360.utility.CommanMethod;

public class CreateAdminPositive extends BaseclassWeb {
	CheckinCommon checkinCommonMethod = new CheckinCommon();
	CommanMethod commonMethod = new CommanMethod();

	@Test(priority = 1)
	public void validLogin() throws InterruptedException {
		LoginFactory Login = PageFactory.initElements(driver, LoginFactory.class);
		AdminFactory Admin = PageFactory.initElements(driver, AdminFactory.class);
		CommonFactory common = PageFactory.initElements(driver, CommonFactory.class);
		
		Thread.sleep(2000);

		// login
		commonMethod.credentials(Login);

		// Go to users page
		commonMethod.goto_UsersPage(driver, common);

		// go to admin page
		Admin.getPage_Admin().click();

		// Create new admin
		Admin.getButton_NewAdmin().click();
		Thread.sleep(2000);

		// select values
		common.getFirstName().sendKeys("Mangesh");
		common.getLastName().sendKeys("Darji");
		Admin.getInput_Email().sendKeys("m.d6@gmail.com");
		Admin.getInput_Mobile().sendKeys("+919983413914");
		Admin.getDrpdwn_Role().click();
		Admin.getDrpdwn_Role().sendKeys(Keys.ENTER);
		Admin.getDrpdwn_Location().click();
		Admin.getDrpdwn_Location().sendKeys(Keys.ENTER);
		Admin.getButton_Submit().click();
		driver.switchTo().activeElement();
		Thread.sleep(2000);
		Admin.button_ModalLinkSentOk.click();
		Thread.sleep(2000);
		String actualSuccessMsg = Admin.getMessage_Success().getText();
		commonMethod.successVerify(actualSuccessMsg);

	}
}