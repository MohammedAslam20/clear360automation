package com.web.clear360.testcase;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.web.clear360.factory.LoginFactory;
import com.web.clear360.factory.StaffFactory;
import com.web.clear360.factory.UploadCsvFactory;
import com.web.clear360.utility.BaseclassWeb;
import com.web.clear360.utility.CommanMethod;

	public class StaffForm extends BaseclassWeb {
		
		
	
		@Test(priority=1)
		public void StaffPositive() throws InterruptedException, IOException {

			LoginFactory Login = PageFactory.initElements(driver, LoginFactory.class);
			UploadCsvFactory Upload = PageFactory.initElements(driver, UploadCsvFactory.class);
			StaffFactory staff = PageFactory.initElements(driver, StaffFactory.class);
			Login.getInput_UserName().sendKeys("mohammed.aslam+oo@aurigait.com");
			Login.getInput_Password().sendKeys("Aslam123*");
			Login.getButton_Login().click();
			Upload.getClick_Menu().click();
		    Upload.getTab_ManageUsers().click();
			staff.getButton_NewStaff().click();
			//Thread.sleep(1000);

		
			
			  staff.getFirstName().sendKeys("Staff");
			  staff.getLastName().sendKeys("Positivetest");
			  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			  staff.getMobile().sendKeys("+919485603205");
			  
			  Select Grade = new Select(driver.findElement(By.id("departments")));
			  Grade.selectByVisibleText("Maths");
			  Select School = new Select(driver.findElement(By.id("locations")));
			  School.selectByVisibleText("Ram Nagar");
			  staff.getUniqueId().sendKeys("Aaa");
			  staff.getBtn_Submit().click();
		
		}
		@Test(priority=2)
		public void StaffMobileReq() throws InterruptedException, IOException {
			StaffFactory staff = PageFactory.initElements(driver, StaffFactory.class);
			 staff.getButton_NewStaff().click();
			 staff.getFirstName().sendKeys("Staff");
			  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			  staff.getLastName().sendKeys("Mobile");
			  staff.getUniqueId().sendKeys("Ab");
			  staff.getBtn_Submit().click();
			  CommanMethod.checkValid(driver);
			  
		}
		@Test(priority=3)
		public void StaffLoc() throws InterruptedException, IOException {
			StaffFactory staff = PageFactory.initElements(driver, StaffFactory.class);
			 driver.findElement(By.xpath("//a[contains(text(),'Back')]")).click();
			 staff.getButton_NewStaff().click();
			 staff.getFirstName().sendKeys("Satff");
			  staff.getLastName().sendKeys("Loc");
			  staff.getMobile().sendKeys("+919485603201");
			  staff.getUniqueId().sendKeys("Ac");
			  staff.getBtn_Submit().click();
			  CommanMethod.checkValid(driver);
			  
		}
		@Test(priority=4)
		public void StaffDepart() throws InterruptedException, IOException {
			StaffFactory staff = PageFactory.initElements(driver, StaffFactory.class);
			 driver.findElement(By.xpath("//a[contains(text(),'Back')]")).click();
			 staff.getButton_NewStaff().click();
			 staff.getFirstName().sendKeys("Staff");
			  staff.getLastName().sendKeys("Depart");
			  staff.getMobile().sendKeys("+919485603202");
			  Select School = new Select(driver.findElement(By.id("locations")));
			  School.selectByVisibleText("Ram Nagar");
			  staff.getUniqueId().sendKeys("Ad");
			  staff.getBtn_Submit().click();
			  CommanMethod.checkValid(driver);
}
		@Test(priority=5)
		public void StaffUniqueIdalredyused() throws InterruptedException, IOException {
			StaffFactory staff = PageFactory.initElements(driver, StaffFactory.class);
			 driver.findElement(By.xpath("//a[contains(text(),'Back')]")).click();
			 staff.getButton_NewStaff().click();
			 staff.getFirstName().sendKeys("Staff");
			  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			  staff.getLastName().sendKeys("UniqueID");
			  staff.getMobile().sendKeys("+919485603203");
			  staff.getUniqueId().sendKeys("Aaa");
			  staff.getBtn_Submit().click();
			  CommanMethod.checkValid(driver);
}
		@Test(priority=6)
		public void MobileNumberIdalredyused() throws InterruptedException, IOException {
			StaffFactory staff = PageFactory.initElements(driver, StaffFactory.class);
			 driver.findElement(By.xpath("//a[contains(text(),'Back')]")).click();
			 staff.getButton_NewStaff().click();
			 staff.getFirstName().sendKeys("Staff");
			  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			  staff.getLastName().sendKeys("MobileAlready");
			  staff.getMobile().sendKeys("+919485603205");
			  staff.getUniqueId().sendKeys("staft");
			  staff.getBtn_Submit().click();
			  CommanMethod.checkValid(driver);
}
	}
