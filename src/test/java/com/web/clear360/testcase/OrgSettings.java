package com.web.clear360.testcase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.web.clear360.factory.CommonFactory;
import com.web.clear360.factory.LoginFactory;
import com.web.clear360.factory.SuperAdminFactory;
import com.web.clear360.utility.BaseclassWeb;
import com.web.clear360.utility.CommanMethod;
import com.web.clear360.utility.SuperAdminCommon;

public class OrgSettings extends BaseclassWeb {

	CommanMethod commonMethod = new CommanMethod();
	SuperAdminCommon superadmincommon = new SuperAdminCommon();
	public String loginEmail = "mangesh.darji22@aurigait.com";
	public String password = "Mangesh@123";
	WebElement StudentSingularLabel, ParentPluralLabel, StudentPluralLabel, ParentSingularLabel, FacultySingularLabel, FacultyPluralLabel, EmployeeSingularLabel,
	EmployeePluralLabel, LocationSingularLabel, LocationPluralLabel, GroupSingularLabel, GroupPluralLabel, 
	TagSingularLabel, TagPluralLabel, OwnerLabel, AdminLabel, LocationManagerLabel, ManualCheckinLabel, 
	SettingsManagerLabel, UserManagerLabel, VisitorManagerLabel, TestingManagerLabel, 
	DeptLabelEmployeeSingular, DeptLabelEmployeePlural, DeptLabelFacultySingular, 
	DeptLabelFacultyPlural, DeptLabelStudentSingular, DeptLabelStudentPlural,
	DeptLabelParentSingular, DeptLabelParentPlural;

	@Test(priority = 1)
	public void validLogin() throws InterruptedException {
		LoginFactory Login = PageFactory.initElements(driver, LoginFactory.class);
		CommonFactory common = PageFactory.initElements(driver, CommonFactory.class);
		SuperAdminFactory superadmin = PageFactory.initElements(driver, SuperAdminFactory.class);

		Thread.sleep(2000);
		commonMethod.credentialSuperAdmin(Login);
		;
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//h6[normalize-space()='Financial Overview']")));

		// go to manage org page_Admin
		commonMethod.goto_OrgPage(driver, common);
		Thread.sleep(2000);

		superadmin.getPagination().click();
		Thread.sleep(2000);
		superadmin.getOrgSettingsPage().click();
		
		Select Drpdwn_AutoMode = new Select(superadmin.getAutoMode());
		Drpdwn_AutoMode.selectByVisibleText("No");
		
		Select Drpdwn_StatusOverride = new Select(superadmin.getStatusOverride());
		Drpdwn_StatusOverride.selectByVisibleText("No");
		
		Select Drpdwn_MinAge = new Select(superadmin.getMinAge());
		Drpdwn_MinAge.selectByVisibleText("19");
		
		Select Drpdwn_CaseMgmtStayHome = new Select(superadmin.getCaseMgmtStayHome());
		Drpdwn_CaseMgmtStayHome.selectByVisibleText("Disabled");
		
		Select Drpdwn_Immunization = new Select(superadmin.getImmunization());
		Drpdwn_Immunization.selectByVisibleText("Disabled");
		
		Select Drpdwn_AdvancedTesting = new Select(superadmin.getAdvancedTesting());
		Drpdwn_AdvancedTesting.selectByVisibleText("Disabled");
		
		Select Drpdwn_EmailLogo = new Select(superadmin.getEmailLogo());
		Drpdwn_EmailLogo.selectByVisibleText("Disabled");
		
		Select Drpdwn_CheckIn = new Select(superadmin.getCheckIn());
		Drpdwn_CheckIn.selectByVisibleText("Enabled");
		
		Select Drpdwn_ContactTracing = new Select(superadmin.getContactTracing());
		Drpdwn_ContactTracing.selectByVisibleText("Disabled");
		
		Select Drpdwn_DailyCheckin = new Select(superadmin.getDailyCheckin());
		Drpdwn_DailyCheckin.selectByVisibleText("Disabled");
		
		
		
		StudentSingularLabel = superadmin.getStudentSingularLabel();
		StudentSingularLabel.sendKeys("test1");
		StudentPluralLabel = superadmin.getStudentPluralLabel();
		StudentPluralLabel.sendKeys("test1");
		ParentSingularLabel = superadmin.getParentSingularLabel();
		ParentSingularLabel.sendKeys("test1");
		ParentPluralLabel = superadmin.getParentPluralLabel();
		ParentPluralLabel.sendKeys("test1");
		FacultySingularLabel = superadmin.getFacultySingularLabel();
		FacultySingularLabel.sendKeys("test1");
		FacultyPluralLabel = superadmin.getFacultyPluralLabel();
		FacultyPluralLabel.sendKeys("test1");
		EmployeeSingularLabel = superadmin.getEmployeeSingularLabel();
		EmployeeSingularLabel.sendKeys("test1");
		EmployeePluralLabel = superadmin.getEmployeePluralLabel();
		EmployeePluralLabel.sendKeys("test1");
		LocationSingularLabel = superadmin.getLocationSingularLabel();
		LocationSingularLabel.sendKeys("test1");
		LocationPluralLabel = superadmin.getLocationPluralLabel();
		LocationPluralLabel.sendKeys("test1");
		GroupSingularLabel = superadmin.getGroupSingularLabel();
		GroupSingularLabel.sendKeys("test1");
		GroupPluralLabel = superadmin.getGroupPluralLabel();
		GroupPluralLabel.sendKeys("test1");
		TagSingularLabel = superadmin.getTagSingularLabel();
		TagSingularLabel.sendKeys("test1");
		TagPluralLabel = superadmin.getTagPluralLabel();
		TagPluralLabel.sendKeys("test1");
		OwnerLabel = superadmin.getOwnerLabel();
		OwnerLabel.sendKeys("test1");
		AdminLabel = superadmin.getAdminLabel();
		AdminLabel.sendKeys("test1");
		LocationManagerLabel = superadmin.getLocationManagerLabel();
		LocationManagerLabel.sendKeys("test1");
		ManualCheckinLabel = superadmin.getManualCheckinLabel();
		ManualCheckinLabel.sendKeys("test1");
		SettingsManagerLabel = superadmin.getSettingsManagerLabel();
		SettingsManagerLabel.sendKeys("test1");
		UserManagerLabel = superadmin.getUserManagerLabel();
		UserManagerLabel.sendKeys("test1");
		VisitorManagerLabel = superadmin.getVisitorManagerLabel();
		VisitorManagerLabel.sendKeys("test1");
		TestingManagerLabel = superadmin.getTestingManagerLabel();
		TestingManagerLabel.sendKeys("test1");
		DeptLabelEmployeeSingular = superadmin.getDeptLabelEmployeeSingular();
		DeptLabelEmployeeSingular.sendKeys("test1");
		DeptLabelEmployeePlural = superadmin.getDeptLabelEmployeePlural();
		DeptLabelEmployeePlural.sendKeys("test1");
		DeptLabelFacultySingular = superadmin.getDeptLabelFacultySingular();
		DeptLabelFacultySingular.sendKeys("test1");
		DeptLabelFacultyPlural = superadmin.getDeptLabelFacultyPlural();
		DeptLabelFacultyPlural.sendKeys("test1");
		DeptLabelStudentSingular = superadmin.getDeptLabelStudentSingular();
		DeptLabelStudentSingular.sendKeys("test1");
		DeptLabelStudentPlural = superadmin.getDeptLabelStudentPlural();
		DeptLabelStudentPlural.sendKeys("test1");
		DeptLabelParentSingular = superadmin.getDeptLabelParentSingular();
		DeptLabelParentSingular.sendKeys("test1");
		DeptLabelParentPlural = superadmin.getDeptLabelParentPlural();
		DeptLabelParentPlural.sendKeys("test1");
		
		superadmin.getButton_Save().click();
		
		Thread.sleep(2000);
		String actualMsg = superadmin.getSuccessMessage().getText();
		CommanMethod.successVerify(actualMsg);
		
	}

	/*
	 * @AfterTest public void terminateBrowser() { driver.close(); }
	 */

}
