package com.web.clear360.testcase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.web.clear360.factory.AdminFactory;
import com.web.clear360.factory.CommonFactory;
import com.web.clear360.factory.LoginFactory;
import com.web.clear360.factory.MessageTempFactory;
import com.web.clear360.factory.TagsFactory;
import com.web.clear360.utility.AdminCommon;
import com.web.clear360.utility.BaseclassWeb;
import com.web.clear360.utility.CommanMethod;

public class CreateMessageTempPositive extends BaseclassWeb {
	CommanMethod commonMethod = new CommanMethod();
	WebElement title, subjectLine, messageBody, buttonSubmit;

	@Test(priority = 1)
	public void forUsers() throws InterruptedException {
		LoginFactory Login = PageFactory.initElements(driver, LoginFactory.class);
		MessageTempFactory MessagesTemp = PageFactory.initElements(driver, MessageTempFactory.class);
		CommonFactory common = PageFactory.initElements(driver, CommonFactory.class);

		Thread.sleep(2000);

		// login
		commonMethod.credentials(Login);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		// Go to Message Template page
		commonMethod.goto_MessageTempPage(driver, common);
		MessagesTemp.getPage_MessageTemp().click();

		// Create new Message Template
		MessagesTemp.getButton_NewMessageTemp().click();

		// select Message type
		Select Drpdwn_MessageType = new Select(MessagesTemp.getDrpdwn_MessageType());
		Drpdwn_MessageType.selectByVisibleText("For Users");

		// Select user type
		MessagesTemp.getDrpdwn_UserType().click();
		MessagesTemp.getDrpdwn_UserType().sendKeys(Keys.ENTER);
		MessagesTemp.getClickOutside().click();

		// Enter values
		title = MessagesTemp.getTitle();
		title.sendKeys("user1");
		subjectLine = MessagesTemp.getSubjectLine();
		subjectLine.sendKeys("test1");
		messageBody = MessagesTemp.getMessageBody();
		messageBody.sendKeys("test1");
		MessagesTemp.getButtonSubmit().click();
		Thread.sleep(2000);

		String actualMsg = MessagesTemp.getSuccessMessage().getText();
		CommanMethod.successVerify(actualMsg);

	}

	@Test(priority = 2)
	public void forAdministrators() throws InterruptedException {
		MessageTempFactory MessagesTemp = PageFactory.initElements(driver, MessageTempFactory.class);
		Thread.sleep(2000);

		// Create new Message Template
		MessagesTemp.getButton_NewMessageTemp().click();

		// select Role type
		Select Drpdwn_MessageType = new Select(MessagesTemp.getDrpdwn_MessageType());
		Drpdwn_MessageType.selectByVisibleText("For Administrators");

		// select roles
		MessagesTemp.getDrpdwn_RoleType().click();
		MessagesTemp.getDrpdwn_RoleType().sendKeys(Keys.ENTER);
		MessagesTemp.getClickOutside().click();

		// select trigger event
		Select Drpdwn_TriggerEvent = new Select(MessagesTemp.getDrpdwn_TriggerEvent());
		Drpdwn_TriggerEvent.selectByVisibleText("User Becomes High Risk");

		// Enter values
		title = MessagesTemp.getTitle();
		title.sendKeys("admin1");
		/*
		 * subjectLine = MessagesTemp.getSubjectLine(); subjectLine.sendKeys("test1");
		 * messageBody = MessagesTemp.getMessageBody(); messageBody.sendKeys("test1");
		 */
		MessagesTemp.getButtonSubmit().click();
		Thread.sleep(2000);

		String actualMsg = MessagesTemp.getSuccessMessage().getText();
		CommanMethod.successVerify(actualMsg);

	}

}