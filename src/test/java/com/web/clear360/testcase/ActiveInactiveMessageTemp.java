package com.web.clear360.testcase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.web.clear360.factory.AdminFactory;
import com.web.clear360.factory.CommonFactory;
import com.web.clear360.factory.LoginFactory;
import com.web.clear360.factory.MessageTempFactory;
import com.web.clear360.factory.TagsFactory;
import com.web.clear360.utility.AdminCommon;
import com.web.clear360.utility.BaseclassWeb;
import com.web.clear360.utility.CommanMethod;

public class ActiveInactiveMessageTemp extends BaseclassWeb {
	CommanMethod commonMethod = new CommanMethod();
	WebElement title, subjectLine, messageBody, buttonSubmit;

	@Test(priority = 1)
	public void forUsers() throws InterruptedException {
		LoginFactory Login = PageFactory.initElements(driver, LoginFactory.class);
		MessageTempFactory MessagesTemp = PageFactory.initElements(driver, MessageTempFactory.class);
		CommonFactory common = PageFactory.initElements(driver, CommonFactory.class);

		Thread.sleep(2000);

		// login
		commonMethod.credentials(Login);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		// Go to Message Template page
		commonMethod.goto_MessageTempPage(driver, common);
		MessagesTemp.getPage_MessageTemp().click();

		// Create new Message Template
		MessagesTemp.getActiveInactiveToggle().click();

	}
}