package com.web.clear360.testcase;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import com.web.clear360.factory.LoginFactory;
import com.web.clear360.utility.BaseclassWeb;
import com.web.clear360.utility.CommanMethod;

public class AdminLoginPositiveTest extends BaseclassWeb {

	CommanMethod commoncodeobj = new CommanMethod();

	@Test(priority = 1)
	public void validLogin() throws InterruptedException {
		LoginFactory Login = PageFactory.initElements(driver, LoginFactory.class);
		Thread.sleep(2000);
		Login.input_UserName.sendKeys("mohammed.aslam+oo@aurigait.com");
		Login.input_Password.sendKeys("Aslam123*");
		Login.button_Login.click();
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//body/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/form[1]/div[1]/div[1]/div[1]/div[1]/input[1]")));
	}

	@AfterTest
	public void terminateBrowser() {
		driver.close();
	}

}
