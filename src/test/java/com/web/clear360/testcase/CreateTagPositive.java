package com.web.clear360.testcase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.web.clear360.factory.AdminFactory;
import com.web.clear360.factory.CommonFactory;
import com.web.clear360.factory.LoginFactory;
import com.web.clear360.factory.TagsFactory;
import com.web.clear360.utility.AdminCommon;
import com.web.clear360.utility.BaseclassWeb;
import com.web.clear360.utility.CommanMethod;

public class CreateTagPositive extends BaseclassWeb {
	AdminCommon adminCommonMethod = new AdminCommon();
	CommanMethod commonMethod = new CommanMethod();
	WebElement tagname, tagcode, email, mobile, role, loc;

	@Test(priority = 1)
	public void submittInvalidFname() throws InterruptedException {
		LoginFactory Login = PageFactory.initElements(driver, LoginFactory.class);
		TagsFactory Tags = PageFactory.initElements(driver, TagsFactory.class);
		CommonFactory common = PageFactory.initElements(driver, CommonFactory.class);

		Thread.sleep(2000);

		// login
		commonMethod.credentials(Login);

		// Go to tags page
		commonMethod.goto_TagsPage(driver, common);
		Tags.getPage_Tags().click();
		
		  // Create new tag 
		Tags.getButton_NewTag().click(); 
		
		//Enter values 
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		tagname = Tags.getTagName(); 
		tagname.sendKeys("tag1"); 
		tagcode = Tags.getTagCode(); 
		tagcode.sendKeys("tag1"); 	
		  
		  }
		  
		  
}