package com.web.clear360.testcase;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import com.web.clear360.factory.LoginFactory;
import com.web.clear360.utility.BaseclassWeb;
import com.web.clear360.utility.CommanMethod;

public class AdminLoginNegativeTest extends BaseclassWeb {

	CommanMethod commoncodeobj = new CommanMethod();

	@Test(priority = 1)
	public void bothBlank() throws InterruptedException {
		commoncodeobj.loadurl(driver);
		LoginFactory Login = PageFactory.initElements(driver, LoginFactory.class);
		Thread.sleep(2000);
		Login.input_UserName.sendKeys("");
		Login.input_Password.sendKeys("");
		Login.button_Login.click();
		CommanMethod.loginError(driver);
	}

	@Test(priority = 2)
	public void PasswordBlank() {
		commoncodeobj.loadurl(driver);
		LoginFactory Login = PageFactory.initElements(driver, LoginFactory.class);
		Login.input_UserName.sendKeys("mohammed.aslam+oo@aurigait.com");
		Login.input_Password.sendKeys("");
		Login.button_Login.click();
		CommanMethod.loginError(driver);

	}

	@Test(priority = 3)
	public void usernameBlank() {
		commoncodeobj.loadurl(driver);
		LoginFactory Login = PageFactory.initElements(driver, LoginFactory.class);
		Login.input_UserName.sendKeys("");
		Login.input_Password.sendKeys("Aslam123*");
		Login.button_Login.click();
		CommanMethod.loginError(driver);
	}

	@Test(priority = 4)
	public void PasswordInvalid() {
		commoncodeobj.loadurl(driver);
		LoginFactory Login = PageFactory.initElements(driver, LoginFactory.class);
		Login.input_UserName.sendKeys("mohammed.aslam+oo@aurigait.com");
		Login.input_Password.sendKeys("Aslam");
		Login.button_Login.click();
		CommanMethod.loginError(driver);
	}

	@Test(priority = 5)
	public void usernameInvalid() {
		commoncodeobj.loadurl(driver);
		LoginFactory Login = PageFactory.initElements(driver, LoginFactory.class);
		Login.input_UserName.sendKeys("mohammed");
		Login.input_Password.sendKeys("Aslam123*");
		Login.button_Login.click();
		CommanMethod.loginError(driver);
	}

	@Test(priority = 6)
	public void bothInvalid() {
		commoncodeobj.loadurl(driver);
		LoginFactory Login = PageFactory.initElements(driver, LoginFactory.class);
		Login.input_UserName.sendKeys("mohammed");
		Login.input_Password.sendKeys("Aslam");
		Login.button_Login.click();
		CommanMethod.loginError(driver);
	}

	@AfterTest
	public void terminateBrowser() {
		driver.close();
	}

}
