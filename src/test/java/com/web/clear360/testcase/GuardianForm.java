package com.web.clear360.testcase;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.web.clear360.factory.LoginFactory;
import com.web.clear360.factory.StaffFactory;
import com.web.clear360.factory.UploadCsvFactory;
import com.web.clear360.utility.BaseclassWeb;
import com.web.clear360.utility.CommanMethod;

	public class GuardianForm extends BaseclassWeb {

		@Test(priority=1)
		public void GuardianPositive() throws InterruptedException, IOException {

			LoginFactory Login = PageFactory.initElements(driver, LoginFactory.class);
			UploadCsvFactory Upload = PageFactory.initElements(driver, UploadCsvFactory.class);
			StaffFactory staff = PageFactory.initElements(driver, StaffFactory.class);
			Thread.sleep(1000);

			
			Login.getInput_UserName().sendKeys("mohammed.aslam+oo@aurigait.com");
			Login.getInput_Password().sendKeys("Aslam123*");
			Login.getButton_Login().click();
			driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
			  Upload.getClick_Menu().click();
			
			  
			  Upload.getTab_ManageUsers().click();
			 
			  Upload.getTab_Guardians().click();
			  /*staff.getBtn_NewGuardian().click();
			  
			  staff.getFirstName().sendKeys("Guardian");
			  staff.getLastName().sendKeys("Positivetest");
			  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			  staff.getMobile().sendKeys("+919485205205");
			  
			  Select Grade = new Select(driver.findElement(By.id("departments")));
			  Grade.selectByVisibleText("Maths");
			  Select School = new Select(driver.findElement(By.id("locations")));
			  School.selectByVisibleText("Ram Nagar");
			  staff.getUniqueId().sendKeys("Gaa");
			  staff.getBtn_Submit().click();*/
		
			//  driver.findElement(By.xpath("//span[contains(text(),'Guardians')]")).click();
			//  driver.findElement(By.xpath("//button[contains(text(),'Create New Guardian')]")).click();
			 /* driver.findElement(By.id("firstName")).sendKeys("urmila");
			  driver.findElement(By.id("lastName")).sendKeys("kumari");
			  
			  driver.findElement(By.id("mobile")).sendKeys("+919485623100");
			 
			 
			  Select Grade = new Select(driver.findElement(By.id("departments")));
			  Grade.selectByVisibleText("Maths");
			  Select School = new Select(driver.findElement(By.id("locations")));
			  School.selectByVisibleText("Ram Nagar");
			  driver.findElement(By.id("uniqueId")).sendKeys("urmi");
			  driver.findElement(By.xpath("//button[contains(text(),'Submit')]")).click();*/
		}
		/*@Test(priority=2)
		public void GuardianMobileReq() throws InterruptedException, IOException {
			StaffFactory staff = PageFactory.initElements(driver, StaffFactory.class);
			 staff.getBtn_NewGuardian().click();
			 staff.getFirstName().sendKeys("Guard");
			  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			  staff.getLastName().sendKeys("Mobile");
			  staff.getUniqueId().sendKeys("Ab");
			  staff.getBtn_Submit().click();
			  CommanMethod.checkValid(driver);
			  
		}*/
		@Test(priority=3)
		public void GuardianLoc() throws InterruptedException, IOException {
			StaffFactory staff = PageFactory.initElements(driver, StaffFactory.class);
			 driver.findElement(By.xpath("//a[contains(text(),'Back')]")).click();
			 staff.getBtn_NewGuardian().click();
			 staff.getFirstName().sendKeys("Guard");
			  staff.getLastName().sendKeys("Loc");
			  staff.getMobile().sendKeys("+919485206207");
			  staff.getUniqueId().sendKeys("Acc");
			  staff.getBtn_Submit().click();
			  CommanMethod.checkValid(driver);
			  
		}
		@Test(priority=4)
		public void GuardDepart() throws InterruptedException, IOException {
			StaffFactory staff = PageFactory.initElements(driver, StaffFactory.class);
			 driver.findElement(By.xpath("//a[contains(text(),'Back')]")).click();
			 staff.getBtn_NewGuardian().click();
			 staff.getFirstName().sendKeys("Guard");
			  staff.getLastName().sendKeys("Depart");
			  staff.getMobile().sendKeys("+919485205208");
			  Select School = new Select(driver.findElement(By.id("locations")));
			  School.selectByVisibleText("Ram Nagar");
			  staff.getUniqueId().sendKeys("Add");
			  staff.getBtn_Submit().click();
			  CommanMethod.checkValid(driver);
}
		@Test(priority=5)
		public void GuardUniqueIdalredyused() throws InterruptedException, IOException {
			StaffFactory staff = PageFactory.initElements(driver, StaffFactory.class);
			 driver.findElement(By.xpath("//a[contains(text(),'Back')]")).click();
			 staff.getBtn_NewGuardian().click();
			 staff.getFirstName().sendKeys("Staff");
			  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			  staff.getLastName().sendKeys("UniqueID");
			  staff.getMobile().sendKeys("+919485206207");
			  staff.getUniqueId().sendKeys("Gaa");
			  staff.getBtn_Submit().click();
			  CommanMethod.checkValid(driver);
}
		@Test(priority=6)
		public void MobileNumberIdalredyused() throws InterruptedException, IOException {
			StaffFactory staff = PageFactory.initElements(driver, StaffFactory.class);
			 driver.findElement(By.xpath("//a[contains(text(),'Back')]")).click();
			 staff.getBtn_NewGuardian().click();
			 staff.getFirstName().sendKeys("Staff");
			  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			  staff.getLastName().sendKeys("MobileAlready");
			  staff.getMobile().sendKeys("+919485205205");
			  staff.getUniqueId().sendKeys("Ac");
			  staff.getBtn_Submit().click();
			  CommanMethod.checkValid(driver);
}
}
