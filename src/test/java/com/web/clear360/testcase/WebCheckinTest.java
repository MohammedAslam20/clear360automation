package com.web.clear360.testcase;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.web.clear360.factory.CheckinFactory;
import com.web.clear360.factory.CommonFactory;
import com.web.clear360.factory.LoginFactory;
import com.web.clear360.utility.BaseclassWeb;
import com.web.clear360.utility.CheckinCommon;
import com.web.clear360.utility.CommanMethod;

public class WebCheckinTest extends BaseclassWeb {
	CheckinCommon checkinCommonMethod = new CheckinCommon();
	public String actualDate = "21/10/2020";
	CommanMethod commonMethod = new CommanMethod();

	@Test(priority = 1)
	public void webCheckinPositiveTesting() throws InterruptedException {
		LoginFactory Login = PageFactory.initElements(driver, LoginFactory.class);
		CheckinFactory Checkin = PageFactory.initElements(driver, CheckinFactory.class);
		CommonFactory common = PageFactory.initElements(driver, CommonFactory.class);
		Thread.sleep(2000);

		// login
		commonMethod.credentials(Login);

		// Go to checkin page
		Thread.sleep(2000);

		commonMethod.goto_CheckinPage(driver, common);

		// Search unique id and submit checkin
		checkinCommonMethod.search_UniqueId(Checkin);

		// select values
		Select drpPositive = new Select(Checkin.getDrpdwn_PositiveTesting());
		drpPositive.selectByVisibleText("Yes");

		// submit checkin page
		Checkin.getButton_Checkinsubmit().click();

		// Close status modal
		checkinCommonMethod.close_StatusModal(driver, Checkin);

		// Go to dashboard
		commonMethod.goto_DashboardPage(driver, common);

		Checkin.getList_HealthProfile().click();

		String actualCheckinValue = Checkin.getPositiveProfileStatusValue().getText();
		checkinCommonMethod.checkinValueCheck(actualCheckinValue);

		String actualDateTextPositiveTesting = Checkin.getDate_PositiveTesting_HealthProfile().getText();
		System.out.println("Testing Date Submitted on checkin is " + actualDate + "\n"
				+ "Testing Date showing on Health profile is " + actualDateTextPositiveTesting);
	}

	@Test(priority = 2)
	public void webCheckinSymptom() throws InterruptedException {
		CheckinFactory Checkin = PageFactory.initElements(driver, CheckinFactory.class);
		CommonFactory common = PageFactory.initElements(driver, CommonFactory.class);
		Thread.sleep(2000);

		// Go to checkin page
		commonMethod.goto_CheckinPage(driver, common);

		// Search unique id and submit checkin
		checkinCommonMethod.search_UniqueId(Checkin);

		// select values
		Checkin.getDrpdwn_Symptoms().click();
		Checkin.getDrpdwn_Symptoms().sendKeys(Keys.ENTER);

		// submit checkin page
		Checkin.getButton_Checkinsubmit().click();

		// Close status modal
		checkinCommonMethod.close_StatusModal(driver, Checkin);

		// Go to dashboard
		commonMethod.goto_DashboardPage(driver, common);

		Checkin.getList_HealthProfile().click();

		String actualValueSymptoms = Checkin.getSymptomsProfileStatusValue().getText();
		checkinCommonMethod.verifySymptomValue(actualValueSymptoms);

	}

	@Test(priority = 3)
	public void webCheckinTravel() throws InterruptedException {
		CheckinFactory Checkin = PageFactory.initElements(driver, CheckinFactory.class);
		CommonFactory common = PageFactory.initElements(driver, CommonFactory.class);
		Thread.sleep(2000);

		// Go to checkin page
		commonMethod.goto_CheckinPage(driver, common);

		// Search unique id and submit checkin
		checkinCommonMethod.search_UniqueId(Checkin);

		// select values
		Select drpTravel = new Select(Checkin.getDrpdwn_Travel());
		drpTravel.selectByVisibleText("Yes");

		// submit checkin page
		Checkin.getButton_Checkinsubmit().click();

		// Close status modal
		checkinCommonMethod.close_StatusModal(driver, Checkin);

		// Go to dashboard
		commonMethod.goto_DashboardPage(driver, common);

		Checkin.getList_HealthProfile().click();

		String actualCheckinValue = Checkin.getTravelProfileStatusValue().getText();
		checkinCommonMethod.checkinValueCheck(actualCheckinValue);

		String actualDateTextTravel = Checkin.getDate_Travel_HealthProfile().getText();
		System.out.println("Travel Date Submitted on checkin is " + actualDate + "\n"
				+ "Travel Date showing on Health profile is " + actualDateTextTravel);
	}

	@Test(priority = 4)
	public void webCheckinExposure() throws InterruptedException {
		CheckinFactory Checkin = PageFactory.initElements(driver, CheckinFactory.class);
		CommonFactory common = PageFactory.initElements(driver, CommonFactory.class);
		Thread.sleep(2000);

		// Go to checkin page
		commonMethod.goto_CheckinPage(driver, common);

		// Search unique id and submit checkin
		checkinCommonMethod.search_UniqueId(Checkin);

		// select values
		Select drpExposure = new Select(Checkin.getDrpdwn_Exposure());
		drpExposure.selectByVisibleText("Yes");

		// submit checkin page
		Checkin.getButton_Checkinsubmit().click();

		// Close status modal
		checkinCommonMethod.close_StatusModal(driver, Checkin);

		// Go to dashboard
		commonMethod.goto_DashboardPage(driver, common);

		Checkin.getList_HealthProfile().click();

		String actualCheckinValue = Checkin.getExposureProfileStatusValue().getText();
		checkinCommonMethod.checkinValueCheck(actualCheckinValue);

		String actualDateTextExposure = Checkin.getDate_Exposure_HealthProfile().getText();
		System.out.println("Exposure Date Submitted on checkin is " + actualDate + "\n"
				+ "Exposure Date showing on Health profile is " + actualDateTextExposure);
	}

	@Test(priority = 5)
	public void webCheckin_AllNo() throws InterruptedException {
		CheckinFactory Checkin = PageFactory.initElements(driver, CheckinFactory.class);
		CommonFactory common = PageFactory.initElements(driver, CommonFactory.class);
		Thread.sleep(1000);

		// Go to checkin page
		commonMethod.goto_CheckinPage(driver, common);

		// Search unique id and submit checkin
		checkinCommonMethod.search_UniqueId(Checkin);

		// click on submit with all no options
		Checkin.getButton_Checkinsubmit().click();

		// Close status modal
		checkinCommonMethod.close_StatusModal(driver, Checkin);

		// Go to dashboard
		commonMethod.goto_DashboardPage(driver, common);

		Checkin.getList_HealthProfile().click();

		// Verify submitted value
		String checkinValuePositiveTesting = Checkin.getPositiveProfileStatusValue().getText();
		String checkinValueTravel = Checkin.getTravelProfileStatusValue().getText();
		String checkinValueExposure = Checkin.getExposureProfileStatusValue().getText();
		String checkinValueNegativeTesting = Checkin.getExposureProfileStatusValue().getText();

		checkinCommonMethod.checkinAllNo(checkinValuePositiveTesting, checkinValueNegativeTesting, checkinValueTravel,
				checkinValueExposure);

	}

	@Test(priority = 6)
	public void webCheckinAttendance() throws InterruptedException {
		CheckinFactory Checkin = PageFactory.initElements(driver, CheckinFactory.class);
		CommonFactory common = PageFactory.initElements(driver, CommonFactory.class);

		Thread.sleep(2000);

		// Go to checkin page
		commonMethod.goto_CheckinPage(driver, common);
		;

		// Search unique id and submit checkin
		checkinCommonMethod.search_UniqueId(Checkin);

		// select values
		Select drpAttendance = new Select(Checkin.getDrpdwn_Attendance());
		drpAttendance.selectByVisibleText("Online");

		// submit checkin page
		Checkin.getButton_Checkinsubmit().click();

		// Close status modal
		checkinCommonMethod.close_StatusModal(driver, Checkin);

		// Go to Users
		commonMethod.goto_UsersPage(driver, common);

		String actualCheckinValue = Checkin.getStaffAttendanceValue().getText();
		checkinCommonMethod.AttendanceCheck(actualCheckinValue);

	}

}