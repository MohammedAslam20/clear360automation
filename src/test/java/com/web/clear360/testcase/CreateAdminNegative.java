package com.web.clear360.testcase;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import com.web.clear360.factory.AdminFactory;
import com.web.clear360.factory.CommonFactory;
import com.web.clear360.factory.LoginFactory;
import com.web.clear360.utility.AdminCommon;
import com.web.clear360.utility.BaseclassWeb;
import com.web.clear360.utility.CommanMethod;

public class CreateAdminNegative extends BaseclassWeb {
	AdminCommon adminCommonMethod = new AdminCommon();
	CommanMethod commonMethod = new CommanMethod();
	WebElement fname, lname, email, mobile, role, loc;

	@Test(priority = 1)
	public void submittInvalidFname() throws InterruptedException {
		LoginFactory Login = PageFactory.initElements(driver, LoginFactory.class);
		AdminFactory Admin = PageFactory.initElements(driver, AdminFactory.class);
		CommonFactory common = PageFactory.initElements(driver, CommonFactory.class);

		Thread.sleep(2000);

		// login
		commonMethod.credentials(Login);

		// Go to users page
		commonMethod.goto_UsersPage(driver, common);

		// go to admin page
		Admin.getPage_Admin().click();

		// Create new admin
		Admin.getButton_NewAdmin().click();
		Thread.sleep(2000);

		// select values
		fname = common.getFirstName();
		fname.sendKeys("123");
		lname = common.getLastName();
		lname.sendKeys("Darji");
		email = Admin.getInput_Email();
		email.sendKeys("m.d6@gmail.com");
		mobile = Admin.getInput_Mobile();
		mobile.sendKeys("+919983413914");
		role = Admin.getDrpdwn_Role();
		role.click();
		Admin.getDrpdwn_Role().sendKeys(Keys.ENTER);
		loc = Admin.getDrpdwn_Location();
		loc.click();
		Admin.getDrpdwn_Location().sendKeys(Keys.ENTER);
		Admin.getButton_Submit().click();
		Thread.sleep(2000);
		String fnameError = Admin.getErrormsg_Firstname().getText();
		adminCommonMethod.fnameError(fnameError);
		fname.clear();
		lname.clear();

	}

	@Test(priority = 2)
	public void submittInvalidLname() throws InterruptedException {
		AdminFactory Admin = PageFactory.initElements(driver, AdminFactory.class);
		CommonFactory common = PageFactory.initElements(driver, CommonFactory.class);

		Thread.sleep(2000);

		// select values
		common.getFirstName().sendKeys("Mangesh");
		lname = common.getLastName();
		lname.sendKeys("123");
		Admin.getButton_Submit().click();
		Thread.sleep(2000);
		String lnameError = Admin.getErrormsg_Lastname().getText();
		adminCommonMethod.lnameError(lnameError);
		lname.clear();
		mobile = Admin.getInput_Mobile();
		mobile.clear();

	}

	@Test(priority = 3)
	public void submittInvalidMobile() throws InterruptedException {
		AdminFactory Admin = PageFactory.initElements(driver, AdminFactory.class);
		CommonFactory common = PageFactory.initElements(driver, CommonFactory.class);

		Thread.sleep(2000);

		// select values
		common.getLastName().sendKeys("Darji");
		mobile = Admin.getInput_Mobile();
		mobile.sendKeys("123");
		Admin.getButton_Submit().click();
		Thread.sleep(2000);
		String mobileError = Admin.getErrormsg_Mobile().getText();
		adminCommonMethod.mobileError(mobileError);
		email = Admin.getInput_Email();
		email.clear();
		mobile.clear();
	}

	@Test(priority = 4)
	public void submittInvalidEmail() throws InterruptedException {
		AdminFactory Admin = PageFactory.initElements(driver, AdminFactory.class);

		Thread.sleep(2000);

		// select values
		mobile = Admin.getInput_Mobile();
		mobile.sendKeys("+919983413914");
		email = Admin.getInput_Email();
		email.sendKeys("mangesh.admin@aurigait.com");
		Admin.getButton_Submit().click();
		Thread.sleep(2000);
		String emailError = Admin.getErrormsg_email().getText();
		adminCommonMethod.emailError(emailError);
		email.clear();

	}
}