package com.mobileApp.clear360.testcase;

import java.io.IOException;
//import java.io.File;
import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeTest;
//import org.openqa.selenium.interactions.touch.TouchActions;
//import org.openqa.selenium.interactions.touch.TouchActions;
import org.testng.annotations.Test;

import com.mobileApp.clear360.utility.CommanApp;

//import org.openqa.selenium.interactions.HasTouchScreen;

import com.mobileApp.clear360.utility.BaseClassMobile;
import com.web.clear360.factory.HealthdetailFactory;
import com.web.clear360.factory.LoginFactory;
import com.web.clear360.utility.BaseclassWeb;
import com.web.clear360.utility.CommanMethod;

import io.appium.java_client.MobileElement;

//import org.openqa.selenium.By;

//Inherit the property from parent class with extends keyword

public class mobileAppLogin extends BaseClassMobile {

	// public String ClearId;

	// public static AndroidDriver<AndroidElement> driver;

	@Test(priority = 1)
	public void login() throws MalformedURLException, InterruptedException {

		// TODO Auto-generated method stub

		// parent class methods "caps" use in child class "twcLogin"

		// TODO Auto-generated method stub

		// parent class methods "caps" use in child class "twcLogin"
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);

		// Click on "Skip"

		driver.findElementByXPath("//android.widget.TextView[contains(@text,'SKIP')]").click();

		// for wrong credentials (Negative testing)
		/*
		 * driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); driver.
		 * findElementByXPath("//*[@class='android.widget.EditText' and @index='0']").
		 * sendKeys("12345"); driver.manage().timeouts().implicitlyWait(10,
		 * TimeUnit.SECONDS);
		 * 
		 * driver.findElementByXPath("//android.widget.TextView[contains(@text,'Next')]"
		 * ).click();
		 * 
		 * System.out.println ("Please enter valid organization id");
		 * 
		 * 
		 * driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		 */
		// Enter Invalid Mobile no.
		/*
		 * driver.
		 * findElementByXPath("//*[@class='android.widget.EditText' and @index='2']").
		 * sendKeys("8965231"); driver.manage().timeouts().implicitlyWait(5,
		 * TimeUnit.SECONDS);
		 * 
		 * driver.
		 * findElementByXPath("//*[@class='android.view.ViewGroup' and @index='0']").
		 * click();
		 * 
		 * System.out.println ("Please enter valid Phone Number");
		 * 
		 * //Enter un-associated mobile no.
		 * driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); driver.
		 * findElementByXPath("//*[@class='android.widget.EditText' and @index='0']").
		 * sendKeys("10000043-101-102"); driver.
		 * findElementByXPath("//*[@class='android.widget.EditText' and @index='2']").
		 * sendKeys("9024429791"); System.out.println
		 * ("Your account is not associated with any organization id");
		 */

		// Successfully login positive testcase
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		driver.findElementByXPath("//*[@class='android.widget.EditText' and @index='0']").sendKeys("10000082");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		driver.findElementByXPath("//*[@class='android.widget.Spinner' and @index='0']").click();

		driver.findElementByXPath("//*[@class='android.widget.CheckedTextView' and @text='+91']").click();
		driver.findElementByXPath("//*[@class='android.widget.EditText' and @index='2']").sendKeys("9024429799");
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Enter Mobile Number']").click();
		driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Next']").click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		System.out.println("Successfully login");

		// driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS);

		System.out.println("Please Enter OTP");

		// Thread.sleep(8000);

		Thread.sleep(3000);

		// driver.findElementByXPath("//*[@class='android.widget.TextView' and
		// @text='Next']").click();

		// driver.findElementByXPath("//*[@class='android.widget.TextView' and
		// @text='Enter the 6 Digit Code']").click();

		driver.findElementByXPath("//*[@class='android.widget.TextView' and @index='2']").click();
		Thread.sleep(3000);

		driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Login Without OTP']").click();

		// driver.manage().timeouts().implicitlyWait(50,TimeUnit.SECONDS);
		System.out.println("OTP Verify Successfully");
	}
	
	
	@Test(priority = 2)
	public void App_onBoardingFollow() throws InterruptedException {
		// Verify Screen
		Thread.sleep(3000);
		driver.findElementByXPath("//*[@class='android.widget.TextView' and @index='1']").getText();
		driver.findElementByXPath("//*[@class='android.widget.TextView' and @index='3']").getText();

		driver.findElementByXPath("//*[@class='android.view.ViewGroup' and @index='6']").click();

		driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Next']").click();
		Thread.sleep(3000);
		System.out.println("Congrats you're Verified");
		Thread.sleep(3000);

		// Click on "I Accept" button
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='I Accept']").click();

		System.out.println("Accept the Terms so Clear360 Can Start Protecting You");

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Enable Notifications']").click();
		System.out.println("Enable Notifications Successfully");

		Thread.sleep(3000);
		driver.findElementByXPath("//*[@class='android.widget.TextView' and @index='3']").getText();

		driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Next']").click();
		
		
	}
		  
		
		
	@Test(priority = 3)
	public void CheckIn_Symptoms() throws InterruptedException {
		
		  // Check-in
		  
		  Thread.sleep(3000); 
		  driver.findElementByXPath( "//android.view.ViewGroup[@index='0']/android.view.ViewGroup[@index='4']").click();
		  
		  Thread.sleep(3000); 
		  driver.findElementByXPath("//android.view.ViewGroup[@index='1']/android.view.ViewGroup[@index='2']").click();
		  
		  Thread.sleep(1000); 
		  driver.findElementByXPath("//android.view.ViewGroup[@index='2']/android.view.ViewGroup[@index='2']").click();
		  
		  Thread.sleep(1000); 
		  driver.findElementByXPath("//android.view.ViewGroup[@index='5']/android.view.ViewGroup[@index='2']").click();
		  
		  Thread.sleep(1000); 
		  driver.findElementByXPath("//android.view.ViewGroup[@index='0']/android.view.ViewGroup[@index='2']").click();
		  
		  Thread.sleep(1000);
		  
		  // Get text 
		  String Cough = driver.findElementByXPath("//android.view.ViewGroup[@index='1']/android.widget.TextView[@text='Cough or worsening of chronic cough']").getText();
		  System.out.println(Cough);
		  
		  
		/*
		 * Thread.sleep(1000); String CoughStatus =driver.findElementByXPath(
		 * "//android.view.ViewGroup[@index='1']/android.widget.TextView[@text='Yes']").
		 * getText(); System.out.println(CoughStatus);
		 */
		  
		  
		  Thread.sleep(1000); 
		  String fevera=driver.findElementByXPath("//android.view.ViewGroup[@index='0']/android.widget.TextView[@text='Fever']").getText(); 
		  System.out.println(fevera);
		  
		  
		  
		  Thread.sleep(3000); 
		  String feverStatus=driver.findElementByXPath("//android.view.ViewGroup[@index='0']/android.widget.TextView[@index='5']").getText(); 
		  System.out.println(feverStatus);
		  
		  Thread.sleep(1000); 
		  String Breath = driver.findElementByXPath("//android.view.ViewGroup[@index='2']/android.widget.TextView[@text='Shortness of Breath']").getText(); 
		  System.out.println(Breath);
		  
		  Thread.sleep(1000);
		  String Headache = driver.findElementByXPath("//android.view.ViewGroup[@index='3']/android.widget.TextView[@text='Headache']").getText();
		  System.out.println(Headache);
		  
		  Thread.sleep(1000); 
		  String Fatigue = driver.findElementByXPath("//android.view.ViewGroup[@index='4']/android.widget.TextView[@text='Fatigue']").getText(); 
		  System.out.println(Fatigue);
		

		Thread.sleep(1000);
		CommanApp.scrollToAnElementBytExt(driver, "Nausea and vomiting");
		
		
		/*
		 * Thread.sleep(1000); CommanApp.scrollToAnElementBytExt(driver, "Stuffy nose");
		 */
		  
		  
		  Thread.sleep(3000); 
		  driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Save']").click();
	}
	
	@Test(priority = 4)
	public void CheckIn_TestedPositive() throws InterruptedException {
		
		  
		  //Tested positive 
		  Thread.sleep(3000); 
		  driver.findElementByXPath("//android.view.ViewGroup[@index='1']/android.view.ViewGroup[@index='4']").click();
		  
		  Thread.sleep(3000); 
		  driver.findElementByXPath("//android.view.ViewGroup[@index='0']/android.view.ViewGroup[@index='2']").click();
		  
		  Thread.sleep(3000); 
		  driver.findElementByXPath("//android.view.ViewGroup[@index='0']/android.view.ViewGroup[@index='3']").click();
		  
		  Thread.sleep(3000);
		  driver.findElementByXPath("//android.view.View[@index='2']").click();
		  
		  
		  Thread.sleep(3000);
		  driver.findElementByXPath("//android.widget.Button[@index='1']").click();
		  
		  Thread.sleep(1000); 
		  
		  String Positivetext =driver.findElementByXPath("//android.view.ViewGroup[@index='0']/android.widget.TextView[@text='I tested positive for COVID-19']").getText(); 
		  System.out.println(Positivetext);
		  
		  
		  Thread.sleep(1000); 
		  String PositiveDate =driver.findElementByXPath("//android.view.ViewGroup[@index='0']/android.widget.TextView[@index='0']").getText(); 
		  System.out.println(PositiveDate);
		  
		  
		  Thread.sleep(1000);
		  driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Save']").click();
	}
		  
	
	@Test(priority = 5)
	public void CheckIn_Travel() throws InterruptedException {
		  //Travel 
		  Thread.sleep(3000); 
		  driver.findElementByXPath("//android.view.ViewGroup[@index='2']/android.view.ViewGroup[@index='4']").click();
		  
		  Thread.sleep(3000); 
		  driver.findElementByXPath("//android.view.ViewGroup[@index='0']/android.view.ViewGroup[@index='2']").click();
		  
		  Thread.sleep(1000); 
		  driver.findElementByXPath("//android.view.ViewGroup[@index='0']/android.view.ViewGroup[@index='3']").click();
		  
		//date
		  Thread.sleep(1000);
		  driver.findElementByXPath("//android.view.View[@index='3']").click();
		  
		  
		  Thread.sleep(3000);
		  driver.findElementByXPath("//android.widget.Button[@index='1']").click();
		  
		  Thread.sleep(1000); 
		  String Taveltext =driver.findElementByXPath("//android.view.ViewGroup[@index='0']/android.widget.TextView[@text='I have traveled to another country since my last check-in']").getText(); 
		  System.out.println(Taveltext);
		  
		  
		  
		  Thread.sleep(1000); 
		  String TavelDate =driver.findElementByXPath("//android.view.ViewGroup[@index='0']/android.widget.TextView[@index='0']").getText(); 
		  System.out.println(TavelDate);
		  
		  Thread.sleep(1000); 
		  driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Save']").click();
	}
	
	@Test(priority = 6)
	public void CheckIn_Exposure() throws InterruptedException {
		  
		  //Exposure 
		  Thread.sleep(3000); 
		  driver.findElementByXPath("//android.view.ViewGroup[@index='3']/android.view.ViewGroup[@index='4']").click();
		  
		  Thread.sleep(3000); 
		  driver.findElementByXPath("//android.view.ViewGroup[@index='0']/android.view.ViewGroup[@index='2']").click();
		  
		  Thread.sleep(1000); 
		  driver.findElementByXPath("//android.view.ViewGroup[@index='0']/android.view.ViewGroup[@index='3']").click();
		  
		  Thread.sleep(1000);//date
		  driver.findElementByXPath("//android.view.View[@index='4']").click();
		  
		  
		  Thread.sleep(1000);
		  driver.findElementByXPath("//android.widget.Button[@index='1']").click();
		  
		  Thread.sleep(1000); 
		  String Exposuretext =driver.findElementByXPath("//android.view.ViewGroup[@index='0']/android.widget.TextView[@text='I have provided care or been in close contact with a person who is feeling unwell since my last check-in']").getText(); 
		  System.out.println(Exposuretext);
		  
		  Thread.sleep(1000); 
		  String ExposureDate =driver.findElementByXPath("//android.view.ViewGroup[@index='0']/android.widget.TextView[@index='0']").getText(); 
		  System.out.println(ExposureDate);
		  
		  
		  Thread.sleep(1000); 
		  driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Save']").click();
		  
		
	}
		  
		  
		  //Down Arrow
		  
		 /* Thread.sleep(3000); driver.findElementByXPath(
		  "//android.view.ViewGroup[@index='0']/android.view.ViewGroup[@index='2']").
		  click();
		  
		  Thread.sleep(3000);
		  
		  driver.findElementByXPath(
		  "//android.view.ViewGroup[@index='1']/android.view.ViewGroup[@index='2']").
		  click();
		  
		  Thread.sleep(3000); driver.findElementByXPath(
		  "//android.view.ViewGroup[@index='2']/android.view.ViewGroup[@index='2']").
		  click();
		  
		  Thread.sleep(3000);
		  
		  driver.findElementByXPath(
		  "//android.view.ViewGroup[@index='3']/android.view.ViewGroup[@index='2']").
		  click();
		  
		  Thread.sleep(3000); driver.findElementByXPath(
		  "//android.view.ViewGroup[@index='4']/android.view.ViewGroup[@index='2']").
		  click();*/
	
	@Test(priority = 7)
	public void Dashboard() throws InterruptedException {
		  
		  Thread.sleep(3000);
		  CommanApp.scrollToAnElementBytExt(driver, "Submit");
		  
		  Thread.sleep(3000); 
		  driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Submit']").click();
		  
		  Thread.sleep(1000);
		  
		  driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Go to Dashboard']").click();
		  
		  Thread.sleep(1000);
		  driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Profile']").click();
		  
		  Thread.sleep(3000);
		  String ClearId = driver.findElementByXPath("//*[@class='android.widget.EditText' and @index='1']").getText(); 
		  System.out.println(ClearId); 
		  //return ClearId;
		  
		  
	}

		/*
		 * driver.
		 * findElementByXPath("//*[@class='android.widget.TextView' and @index='0']").
		 * getText(); driver.
		 * findElementByXPath("//*[@class='android.widget.TextView' and @index='1']").
		 * getText(); driver.
		 * findElementByXPath("//*[@class='android.widget.TextView' and @text='0']").
		 * getText(); driver.
		 * findElementByXPath("//*[@class='android.widget.TextView' and @text='Next']").
		 * click(); driver.
		 * findElementByXPath("//*[@class='android.widget.TextView' and @text='Next']").
		 * click(); driver.
		 * findElementByXPath("//*[@class='android.widget.TextView' and @text='Next']").
		 * click();
		 * 
		 * //xpath(
		 * "//android.widget.LinearLayout[@index='1']/android.widget.Button[@index='1']")
		 * //
		 * ("//android.view.ViewGroup[@index='0']/android.widget.TextView[@test='Yes']")
		 * //Save Profile
		 * 
		 * Thread.sleep(5000);
		 * 
		 * driver.
		 * findElementByXPath("//*[@class='android.widget.EditText' and @text='Mohammed']"
		 * ).clear(); Thread.sleep(4000); driver.
		 * findElementByXPath("//*[@class='android.widget.EditText' and @text='Aslam']")
		 * .clear(); Thread.sleep(4000); driver.
		 * findElementByXPath("//*[@class='android.widget.EditText' and @text='25']").
		 * clear();
		 * 
		 * 
		 * //driver.
		 * findElementByXPath("//*[@class='android.widget.EditText' and @bounds='[609,501][1314,672]']"
		 * ).clear(); //driver.
		 * findElementByXPath("//*[@class='android.widget.EditText' and @bounds='[609,763][1314,934]']"
		 * ).clear(); //driver.
		 * findElementByXPath("//*[@class='android.widget.EditText' and @bounds='[609,1025][1314,1196]']"
		 * ).clear();
		 * 
		 * 
		 * driver.
		 * findElementByXPath("//*[@class='android.widget.TextView' and @text='Save Profile']"
		 * ).click(); System.out.println
		 * ("All fields are mandatory First name,last name,age,biological sex");
		 * 
		 * 
		 * //First Name Validation
		 * driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS); driver.
		 * findElementByXPath("//*[@class='android.widget.EditText' and @text='Enter first name here']"
		 * ).sendKeys("123firstname"); driver.
		 * findElementByXPath("//*[@class='android.widget.TextView' and @text='Save Profile']"
		 * ).click(); System.out.println
		 * ("Please enter a valid name:must conation between 2-40 characters");
		 * 
		 * driver.
		 * findElementByXPath("//*[@class='android.widget.EditText' and @text='123firstname']"
		 * ).sendKeys("Mohammed");
		 * 
		 * //Last Name Validation
		 * driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS); driver.
		 * findElementByXPath("//*[@class='android.widget.EditText' and @text='Enter last name here']"
		 * ).sendKeys("123Lastname"); driver.
		 * findElementByXPath("//*[@class='android.widget.TextView' and @text='Save Profile']"
		 * ).click();
		 * 
		 * System.out.println
		 * ("Please enter a valid Last name:must conation between 2-40 characters");
		 * 
		 * 
		 * driver.
		 * findElementByXPath("//*[@class='android.widget.EditText' and @text='123Lastname']"
		 * ).sendKeys("Aslam");
		 * 
		 * //Age Validation
		 * 
		 * 
		 * driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS); driver.
		 * findElementByXPath("//*[@class='android.widget.EditText' and @text='Enter your age here']"
		 * ).sendKeys("0"); driver.
		 * findElementByXPath("//*[@class='android.widget.TextView' and @text='Save Profile']"
		 * ).click();
		 * System.out.println("Age field should not accept 0 , Please enter valid Age");
		 * driver.
		 * findElementByXPath("//*[@class='android.widget.EditText' and @text='0']").
		 * sendKeys("25");
		 * 
		 * 
		 * //Fill valid data Thread.sleep(8000);
		 * 
		 * driver.
		 * findElementByXPath("//*[@class='android.widget.EditText' and @bounds='[609,501][1314,672]']"
		 * ).sendKeys("MohammedA"); driver.
		 * findElementByXPath("//*[@class='android.widget.EditText' and @bounds='[609,763][1314,934]']"
		 * ).sendKeys("AslamR"); //driver.
		 * findElementByXPath("//*[@class='android.widget.EditText' and @text='Enter your age here']"
		 * ).click(); //driver.
		 * findElementByXPath("//*[@class='android.widget.EditText' and @text='Enter your age here']"
		 * ).clear(); driver.
		 * findElementByXPath("//*[@class='android.widget.EditText' and @bounds='[609,1025][1314,1196]']"
		 * ).sendKeys("26");
		 * 
		 * driver.
		 * findElementByXPath("//*[@class='android.widget.TextView' and @text='Male']").
		 * click(); driver.
		 * findElementByXPath("//*[@class='android.widget.TextView' and @text='Save Profile']"
		 * ).click(); System.out.println ("Profile Successfuly Saved");
		 * 
		 * 
		 * //I Feel good Thread.sleep(3000); driver.
		 * findElementByXPath("//*[@class='android.view.ViewGroup' and @index='4']").
		 * click(); System.out.println ("Clicked on 'I feel good' Button ");
		 * 
		 * 
		 * //Daily Check-in Screen Thread.sleep(3000); driver.
		 * findElementByXPath("//*[@class='android.view.ViewGroup' and @index='3']").
		 * click(); System.out.println ("Clicked on 'Go to Dashboard' Button ");
		 * System.out.println ("User Successfully Navigate on Dashboard");
		 * 
		 * 
		 * //Get Counter value
		 * 
		 * 
		 * // I don't feel well Thread.sleep(3000); driver.
		 * findElementByXPath("//*[@class='android.view.ViewGroup' and @index='5']").
		 * click(); System.out.println ("Clicked on 'I don't feel well' Button "); }
		 */
	}

