package com.web.clear360.factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.web.clear360.utility.BaseclassWeb;

public class TagsFactory extends BaseclassWeb {

	// This is a constructor, as every page need a base driver to find web elements
	public TagsFactory(WebDriver driver) {
		super();
	}

	@FindBy(how = How.XPATH, using = "//div[@class='nav flex-column nav-pills']//span[contains(text(),'Tags')]")
	public WebElement Page_Tags;


	public WebElement getPage_Tags() {
		return Page_Tags;
	}

	public void setPage_Tags(WebElement page_Tags) {
		Page_Tags = page_Tags;
	}

	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Create New Tag')]")
	public WebElement button_NewTag;


	public WebElement getButton_NewTag() {
		return button_NewTag;
	}

	public void setButton_NewTag(WebElement button_NewTag) {
		this.button_NewTag = button_NewTag;
	}


	public WebElement getTagName() {
		return TagName;
	}

	public void setTagName(WebElement tagName) {
		TagName = tagName;
	}
	
	@FindBy(how = How.XPATH, using = "//input[@id='tagName']")
	public WebElement TagName;


	@FindBy(how = How.XPATH, using = "//input[@id='tagCode']")
	public WebElement TagCode;

	public WebElement getTagCode() {
		return TagCode;
	}

	public void setTagCode(WebElement tagCode) {
		TagCode = tagCode;
	}
	
	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Submit')]")
	public WebElement SubmitTag;


	public WebElement getSubmitTag() {
		return SubmitTag;
	}

	public void setSubmitTag(WebElement submitTag) {
		SubmitTag = submitTag;
	}
}
