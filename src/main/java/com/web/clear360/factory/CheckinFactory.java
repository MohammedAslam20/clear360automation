package com.web.clear360.factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.web.clear360.utility.BaseclassWeb;

public class CheckinFactory extends BaseclassWeb {

	// This is a constructor, as every page need a base driver to find web elements
	public CheckinFactory(WebDriver driver) {
		super();
	}

	public WebElement getInput_CheckinSearch() {
		return input_CheckinSearch;
	}

	public void setInput_CheckinSearch(WebElement input_CheckinSearch) {
		this.input_CheckinSearch = input_CheckinSearch;
	}

	public WebElement getButton_CheckinSearch() {
		return button_CheckinSearch;
	}

	public void setButton_CheckinSearch(WebElement button_CheckinSearch) {
		this.button_CheckinSearch = button_CheckinSearch;
	}

	public WebElement getButton_Checkinsubmit() {
		return button_Checkinsubmit;
	}

	public void setButton_Checkinsubmit(WebElement button_Checkinsubmit) {
		this.button_Checkinsubmit = button_Checkinsubmit;
	}

	@FindBy(how = How.ID, using = "search_text")
	public WebElement input_CheckinSearch;

	@FindBy(how = How.ID, using = "searchUniqueId")
	public WebElement button_CheckinSearch;

	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Submit')]")
	public WebElement button_Checkinsubmit;

	@FindBy(how = How.ID, using = "travel0.value")
	public WebElement drpdwn_Travel;

	public WebElement getDrpdwn_Travel() {
		return drpdwn_Travel;
	}

	public void setDrpdwn_Travel(WebElement drpdwn_Travel) {
		this.drpdwn_Travel = drpdwn_Travel;
	}

	public WebElement getDrpdwn_Exposure() {
		return drpdwn_Exposure;
	}

	public void setDrpdwn_Exposure(WebElement drpdwn_Exposure) {
		this.drpdwn_Exposure = drpdwn_Exposure;
	}

	public WebElement getDrpdwn_Attendance() {
		return drpdwn_Attendance;
	}

	public void setDrpdwn_Attendance(WebElement drpdwn_Attendance) {
		this.drpdwn_Attendance = drpdwn_Attendance;
	}

	@FindBy(how = How.ID, using = "exposure0.value")
	public WebElement drpdwn_Exposure;

	@FindBy(how = How.ID, using = "testing0.value")
	public WebElement drpdwn_PositiveTesting;

	public WebElement getDrpdwn_PositiveTesting() {
		return drpdwn_PositiveTesting;
	}

	public void setDrpdwn_PositiveTesting(WebElement drpdwn_PositiveTesting) {
		this.drpdwn_PositiveTesting = drpdwn_PositiveTesting;
	}

	public WebElement getDrpdwn_NegativeTesting() {
		return drpdwn_NegativeTesting;
	}

	public void setDrpdwn_NegativeTesting(WebElement drpdwn_NegativeTesting) {
		this.drpdwn_NegativeTesting = drpdwn_NegativeTesting;
	}

	@FindBy(how = How.ID, using = "testing1.value")
	public WebElement drpdwn_NegativeTesting;

	@FindBy(how = How.ID, using = "attendanceDto.workingFromWhere")
	public WebElement drpdwn_Attendance;

	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Ok')]")
	public WebElement button_StatusModalOk;

	public WebElement getButton_StatusModalOk() {
		return button_StatusModalOk;
	}

	public void setButton_StatusModalOk(WebElement button_StatusModalOk) {
		this.button_StatusModalOk = button_StatusModalOk;
	}

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Mohammed Aslam test long name Staff')]")
	public WebElement list_HealthProfile;

	public WebElement getList_HealthProfile() {
		return list_HealthProfile;
	}

	public void setList_HealthProfile(WebElement list_HealthProfile) {
		this.list_HealthProfile = list_HealthProfile;
	}

	@FindBy(how = How.XPATH, using = "//tbody/tr[1]/td[2]/span[1]")
	public WebElement PositiveProfileStatusValue;

	public WebElement getPositiveProfileStatusValue() {
		return PositiveProfileStatusValue;
	}

	public void setPositiveProfileStatusValue(WebElement positiveProfileStatusValue) {
		PositiveProfileStatusValue = positiveProfileStatusValue;
	}

	public WebElement getNegativeProfileStatusValue() {
		return NegativeProfileStatusValue;
	}

	public void setNegativeProfileStatusValue(WebElement negativeProfileStatusValue) {
		NegativeProfileStatusValue = negativeProfileStatusValue;
	}

	@FindBy(how = How.XPATH, using = "//tbody/tr[2]/td[2]/span[1]")
	public WebElement NegativeProfileStatusValue;

	@FindBy(how = How.XPATH, using = "//tbody/tr[3]/td[2]/span[1]")
	public WebElement TravelProfileStatusValue;

	public WebElement getTravelProfileStatusValue() {
		return TravelProfileStatusValue;
	}

	public void setTravelProfileStatusValue(WebElement travelProfileStatusValue) {
		TravelProfileStatusValue = travelProfileStatusValue;
	}

	public WebElement getExposureProfileStatusValue() {
		return ExposureProfileStatusValue;
	}

	public void setExposureProfileStatusValue(WebElement exposureProfileStatusValue) {
		ExposureProfileStatusValue = exposureProfileStatusValue;
	}

	@FindBy(how = How.XPATH, using = "//tbody/tr[4]/td[2]/span[1]")
	public WebElement ExposureProfileStatusValue;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Fever')]")
	public WebElement SymptomsProfileStatusValue;

	public WebElement getSymptomsProfileStatusValue() {
		return SymptomsProfileStatusValue;
	}

	public void setSymptomsProfileStatusValue(WebElement symptomsProfileStatusValue) {
		SymptomsProfileStatusValue = symptomsProfileStatusValue;
	}

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Online')]")
	public WebElement StaffAttendanceValue;

	public WebElement getStaffAttendanceValue() {
		return StaffAttendanceValue;
	}

	public void setStaffAttendanceValue(WebElement staffAttendanceValue) {
		StaffAttendanceValue = staffAttendanceValue;
	}

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Select']")
	public WebElement drpdwn_Symptoms;

	public WebElement getDrpdwn_Symptoms() {
		return drpdwn_Symptoms;
	}

	public void setDrpdwn_Symptoms(WebElement drpdwn_Symptoms) {
		this.drpdwn_Symptoms = drpdwn_Symptoms;
	}

	@FindBy(how = How.ID, using = "travel0.date")
	public WebElement date_Travel;

	public WebElement getDate_Travel() {
		return date_Travel;
	}

	public void setDate_Travel(WebElement date_Travel) {
		this.date_Travel = date_Travel;
	}

	public WebElement getDate_Exposure() {
		return date_Exposure;
	}

	public void setDate_Exposure(WebElement date_Exposure) {
		this.date_Exposure = date_Exposure;
	}

	@FindBy(how = How.ID, using = "exposure0.date")
	public WebElement date_Exposure;
	@FindBy(how = How.ID, using = "testing0.date")
	public WebElement date_PositiveTesting;
	@FindBy(how = How.ID, using = "testing0.date")
	public WebElement date_NegativeTesting;

	public WebElement getDate_PositiveTesting() {
		return date_PositiveTesting;
	}

	public void setDate_PositiveTesting(WebElement date_PositiveTesting) {
		this.date_PositiveTesting = date_PositiveTesting;
	}

	public WebElement getDate_NegativeTesting() {
		return date_NegativeTesting;
	}

	public void setDate_NegativeTesting(WebElement date_NegativeTesting) {
		this.date_NegativeTesting = date_NegativeTesting;
	}

	@FindBy(how = How.XPATH, using = "//tbody/tr[3]/td[1]/span[1]")
	public WebElement date_Travel_HealthProfile;

	public WebElement getDate_Travel_HealthProfile() {
		return date_Travel_HealthProfile;
	}

	public void setDate_Travel_HealthProfile(WebElement date_Travel_HealthProfile) {
		this.date_Travel_HealthProfile = date_Travel_HealthProfile;
	}

	public WebElement getDate_Exposure_HealthProfile() {
		return date_Exposure_HealthProfile;
	}

	public void setDate_Exposure_HealthProfile(WebElement date_Exposure_HealthProfile) {
		this.date_Exposure_HealthProfile = date_Exposure_HealthProfile;
	}

	@FindBy(how = How.XPATH, using = "//tbody/tr[1]/td[1]/span[1]")
	public WebElement date_PositiveTesting_HealthProfile;

	public WebElement getDate_PositiveTesting_HealthProfile() {
		return date_PositiveTesting_HealthProfile;
	}

	public void setDate_PositiveTesting_HealthProfile(WebElement date_PositiveTesting_HealthProfile) {
		this.date_PositiveTesting_HealthProfile = date_PositiveTesting_HealthProfile;
	}

	public WebElement getDate_NegativeTesting_HealthProfile() {
		return date_NegativeTesting_HealthProfile;
	}

	public void setDate_NegativeTesting_HealthProfile(WebElement date_NegativeTesting_HealthProfile) {
		this.date_NegativeTesting_HealthProfile = date_NegativeTesting_HealthProfile;
	}

	@FindBy(how = How.XPATH, using = "//tbody/tr[2]/td[1]/span[1]")
	public WebElement date_NegativeTesting_HealthProfile;

	@FindBy(how = How.XPATH, using = "//tbody/tr[4]/td[1]/span[1]")
	public WebElement date_Exposure_HealthProfile;

}
