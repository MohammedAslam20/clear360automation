package com.web.clear360.factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.web.clear360.utility.BaseclassWeb;

public class AdminFactory extends BaseclassWeb {

	// This is a constructor, as every page need a base driver to find web elements
	public AdminFactory(WebDriver driver) {
		super();
	}

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Admins')]")
	public WebElement page_Admin;

	public WebElement getPage_Admin() {
		return page_Admin;
	}

	public void setPage_Admin(WebElement page_Admin) {
		this.page_Admin = page_Admin;
	}
	

	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Create New Admin')]")
	public WebElement button_NewAdmin;

	public WebElement getButton_NewAdmin() {
		return button_NewAdmin;
	}

	public void setButton_NewAdmin(WebElement button_NewAdmin) {
		this.button_NewAdmin = button_NewAdmin;
	}

	@FindBy(how = How.ID, using = "userName")
	public WebElement input_Email;

	public WebElement getInput_Email() {
		return input_Email;
	}

	public void setInput_Email(WebElement input_Email) {
		this.input_Email = input_Email;
	}

	@FindBy(how = How.ID, using = "mobileNo")
	public WebElement input_Mobile;

	public WebElement getInput_Mobile() {
		return input_Mobile;
	}

	public void setInput_Mobile(WebElement input_Mobile) {
		this.input_Mobile = input_Mobile;
	}

	@FindBy(how = How.XPATH, using = "//body/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[3]/div[1]/div[1]/div[2]/form[1]/div[3]/div[1]/span[1]/span[1]/span[1]")
	public WebElement drpdwn_Role;

	public WebElement getDrpdwn_Role() {
		return drpdwn_Role;
	}

	public void setDrpdwn_Role(WebElement drpdwn_Role) {
		this.drpdwn_Role = drpdwn_Role;
	}

	@FindBy(how = How.XPATH, using = "//body/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[3]/div[1]/div[1]/div[2]/form[1]/div[3]/div[2]/span[1]/span[1]/span[1]")
	public WebElement drpdwn_Location;

	public WebElement getDrpdwn_Location() {
		return drpdwn_Location;
	}

	public void setDrpdwn_Location(WebElement drpdwn_Location) {
		this.drpdwn_Location = drpdwn_Location;
	}

	@FindBy(how = How.ID, using = "submitButton")
	public WebElement button_Submit;

	public WebElement getButton_Submit() {
		return button_Submit;
	}

	public void setButton_Submit(WebElement button_Submit) {
		this.button_Submit = button_Submit;
	}

	@FindBy(how = How.XPATH, using = "//button[contains(text(),'OK')]")
	public WebElement button_ModalLinkSentOk;

	public WebElement getButton_ModalLinkSentOk() {
		return button_ModalLinkSentOk;
	}

	public void setButton_ModalLinkSentOk(WebElement button_ModalLinkSentOk) {
		this.button_ModalLinkSentOk = button_ModalLinkSentOk;
	}

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Updated successfully')]")
	public WebElement message_Success;

	public WebElement getMessage_Success() {
		return message_Success;
	}

	public void setMessage_Success(WebElement message_Success) {
		this.message_Success = message_Success;
	}

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Please Enter a Valid First Name.')]")
	public WebElement errormsg_Firstname;

	public WebElement getErrormsg_Firstname() {
		return errormsg_Firstname;
	}

	public void setErrormsg_Firstname(WebElement errormsg_Firstname) {
		this.errormsg_Firstname = errormsg_Firstname;
	}

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Please Enter a Valid Last Name.')]")
	public WebElement errormsg_Lastname;

	public WebElement getErrormsg_Lastname() {
		return errormsg_Lastname;
	}

	public void setErrormsg_Lastname(WebElement errormsg_Lastname) {
		this.errormsg_Lastname = errormsg_Lastname;
	}

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Please Enter Valid Mobile Number')]")
	public WebElement errormsg_Mobile;

	public WebElement getErrormsg_Mobile() {
		return errormsg_Mobile;
	}

	public void setErrormsg_Mobile(WebElement errormsg_Mobile) {
		this.errormsg_Mobile = errormsg_Mobile;
	}

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'User Email Already Exist')]")
	public WebElement errormsg_email;

	public WebElement getErrormsg_email() {
		return errormsg_email;
	}

	public void setErrormsg_email(WebElement errormsg_email) {
		this.errormsg_email = errormsg_email;
	}

}
