package com.web.clear360.factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.web.clear360.utility.BaseclassWeb;

public class MessageTempFactory extends BaseclassWeb {

	// This is a constructor, as every page need a base driver to find web elements
	public MessageTempFactory(WebDriver driver) {
		super();
	}

	@FindBy(how = How.XPATH, using = "//a[normalize-space()='Message Templates']")
	public WebElement Page_MessageTemp;

	public WebElement getPage_MessageTemp() {
		return Page_MessageTemp;
	}

	public void setPage_MessageTemp(WebElement page_MessageTemp) {
		Page_MessageTemp = page_MessageTemp;
	}

	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Create New Message Template']")
	public WebElement button_NewMessageTemp;


	public WebElement getButton_NewMessageTemp() {
		return button_NewMessageTemp;
	}

	public void setButton_NewMessageTemp(WebElement button_NewMessageTemp) {
		this.button_NewMessageTemp = button_NewMessageTemp;
	}

	
	
	@FindBy(how = How.XPATH, using = "//select[@id='messageType']")
	public WebElement Drpdwn_MessageType;

	public WebElement getDrpdwn_MessageType() {
		return Drpdwn_MessageType;
	}

	public void setDrpdwn_MessageType(WebElement drpdwn_MessageType) {
		Drpdwn_MessageType = drpdwn_MessageType;
	}
	
	@FindBy(how = How.XPATH, using = "//select[@id='notificationTriggerEvent']")
	public WebElement Drpdwn_TriggerEvent;
	
	public WebElement getDrpdwn_TriggerEvent() {
		return Drpdwn_TriggerEvent;
	}

	public void setDrpdwn_TriggerEvent(WebElement drpdwn_TriggerEvent) {
		Drpdwn_TriggerEvent = drpdwn_TriggerEvent;
	}

	@FindBy(how = How.XPATH, using = "//div[@id='div-app-user-type']//span[@role='combobox']")				
	public WebElement Drpdwn_UserType;

	public WebElement getDrpdwn_UserType() {
		return Drpdwn_UserType;
	}

	public void setDrpdwn_UserType(WebElement drpdwn_UserType) {
		Drpdwn_UserType = drpdwn_UserType;
	}
	
	@FindBy(how = How.XPATH, using = "//div[@id='div-role-type']//span[@role='combobox']")				
	public WebElement Drpdwn_RoleType;
	
	
	public WebElement getDrpdwn_RoleType() {
		return Drpdwn_RoleType;
	}

	public void setDrpdwn_RoleType(WebElement drpdwn_RoleType) {
		Drpdwn_RoleType = drpdwn_RoleType;
	}

	@FindBy(how = How.XPATH, using = "//label[normalize-space()='Message Type']")
	public WebElement ClickOutside;
	
	
	public WebElement getClickOutside() {
		return ClickOutside;
	}

	public void setClickOutside(WebElement clickOutside) {
		ClickOutside = clickOutside;
	}

	@FindBy(how = How.XPATH, using = "//input[@id='name']")
	public WebElement Title;

	public WebElement getTitle() {
		return Title;
	}

	public void setTitle(WebElement title) {
		Title = title;
	}
	
	@FindBy(how = How.XPATH, using = "//input[@id='notificationSubjectLine']")
	public WebElement SubjectLine;

	public WebElement getSubjectLine() {
		return SubjectLine;
	}

	public void setSubjectLine(WebElement subjectLine) {
		SubjectLine = subjectLine;
	}
	
	@FindBy(how = How.XPATH, using = "//textarea[@id='input-notification-body']")
	public WebElement MessageBody;

	public WebElement getMessageBody() {
		return MessageBody;
	}

	public void setMessageBody(WebElement messageBody) {
		MessageBody = messageBody;
	}
	
	@FindBy(how = How.XPATH, using = "//button[normalize-space()='Save']")
	public WebElement ButtonSubmit;

	public WebElement getButtonSubmit() {
		return ButtonSubmit;
	}

	public void setButtonSubmit(WebElement buttonSubmit) {
		ButtonSubmit = buttonSubmit;
	}
	
	@FindBy(how = How.XPATH, using = "//tbody/tr[1]/td[6]/a[1]/span[1]//*[local-name()='svg']")
	public WebElement ButtonEdit;
	
	
	public WebElement getButtonEdit() {
		return ButtonEdit;
	}

	public void setButtonEdit(WebElement buttonEdit) {
		ButtonEdit = buttonEdit;
	}
	
	@FindBy(how = How.XPATH, using = "//tbody/tr[7]/td[6]/a[1]/span[1]//*[local-name()='svg']")
	public WebElement ButtonAdminEdit;
	
	
	public WebElement getButtonAdminEdit() {
		return ButtonAdminEdit;
	}

	public void setButtonAdminEdit(WebElement buttonAdminEdit) {
		ButtonAdminEdit = buttonAdminEdit;
	}

	@FindBy(how = How.XPATH, using = "//tbody/tr[1]/td[6]/label[1]/span[1]")
	public WebElement ActiveInactiveToggle;
	

	public WebElement getActiveInactiveToggle() {
		return ActiveInactiveToggle;
	}

	public void setActiveInactiveToggle(WebElement activeInactiveToggle) {
		ActiveInactiveToggle = activeInactiveToggle;
	}

	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Updated Successfully']")
	public WebElement SuccessMessage;

	public WebElement getSuccessMessage() {
		return SuccessMessage;
	}

	public void setSuccessMessage(WebElement successMessage) {
		SuccessMessage = successMessage;
	}
}
