package com.web.clear360.factory;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import com.web.clear360.utility.BaseclassWeb;

public class HealthdetailFactory extends BaseclassWeb{

	//This is a constructor, as every page need a base driver to find web elements
	public HealthdetailFactory(WebDriver driver) {
		super();
	}
	
	
	
	@FindBy(how = How.XPATH, using = "//div[@id='filterstopbar-header-first']//input[@id='guardianId']")     
	public WebElement input_Search ;
	
	@FindBy(how = How.XPATH, using = "//div[@id='filterstopbar-header-first']//span[contains(text(),'Search')] ")     
	public WebElement button_Search ;
	
	
	
	@FindBy(how = How.XPATH, using = "//body[@class='menu-position-side menu-side-left full-screen with-content-panel']/div[@class='with-side-panel solid-bg-all']/div[@class='layout-w']/div[@class='content-w']/div[@class='all-wrapper content-i']/div[@class='content-box']/div[@class='row']/div[@class='col-sm-4 d-none1 d-lg-block']/div[2] ")     
	public WebElement tab_Symptoms ;
	
	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Fever')] ")     
	public WebElement tab_Ferver ;
	
	
	  @FindBy(how = How.XPATH, using ="//body[@class='menu-position-side menu-side-left full-screen with-content-panel']/div[@class='with-side-panel solid-bg-all']/div[@class='layout-w']/div[@class='content-w']/div[@class='all-wrapper content-i']/div[@class='content-box']/div[@class='row']/div[@class='col-sm-4 d-none1 d-lg-block']/div[1] ")
	  public WebElement tab_ExposureCOVID19 ;
	  
	  
	
	 
	  public WebElement getTab_ExposureCOVID19() {
		return tab_ExposureCOVID19;
	}

	public void setTab_ExposureCOVID19(WebElement tab_ExposureCOVID19) {
		this.tab_ExposureCOVID19 = tab_ExposureCOVID19;
	}

	public WebElement getTab_TestedPositiveDate() {
		return tab_TestedPositiveDate;
	}

	public void setTab_TestedPositiveDate(WebElement tab_TestedPositiveDate) {
		this.tab_TestedPositiveDate = tab_TestedPositiveDate;
	}

	public WebElement getTab_TestedPositiveStatus() {
		return tab_TestedPositiveStatus;
	}

	public void setTab_TestedPositiveStatus(WebElement tab_TestedPositiveStatus) {
		this.tab_TestedPositiveStatus = tab_TestedPositiveStatus;
	}



	@FindBy(how = How.XPATH, using ="//div[@id='user-exposure']//tr[1]//td[1]")
	  public WebElement tab_TestedPositiveDate;
	  
	  @FindBy(how = How.XPATH, using ="//div[@id='user-exposure']//tr[1]//td[2]//span[1]")
	  public WebElement tab_TestedPositiveStatus;
	  
	  
	
	
	

	public WebElement getInput_Search() {
		return input_Search;
	}

	public void setInput_Search(WebElement input_Search) {
		this.input_Search = input_Search;
	}

	public WebElement getButton_Search() {
		return button_Search;
	}

	public void setButton_Search(WebElement button_Search) {
		this.button_Search = button_Search;
	}

	public WebElement getTab_Symptoms() {
		return tab_Symptoms;
	}

	public void setTab_Symptoms(WebElement tab_Symptoms) {
		this.tab_Symptoms = tab_Symptoms;
	}

	public WebElement getTab_Ferver() {
		return tab_Ferver;
	}

	public void setTab_Ferver(WebElement tab_Ferver) {
		this.tab_Ferver = tab_Ferver;
	}


	
	
	
}
