package com.web.clear360.factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.web.clear360.utility.BaseclassWeb;

public class SuperAdminFactory extends BaseclassWeb {

	// This is a constructor, as every page need a base driver to find web elements
	public SuperAdminFactory(WebDriver driver) {
		super();
	}
	
	@FindBy(how = How.XPATH, using = "//a[normalize-space()='7']")
	public WebElement Pagination;
	

	public WebElement getPagination() {
		return Pagination;
	}

	public void setPagination(WebElement pagination) {
		Pagination = pagination;
	}
	
	
	  @FindBy(how = How.XPATH, using = "//tbody/tr[4]/td[8]/a[1]/span[1]/i[1]")
	  public WebElement OrgSettingsPage;
	  
	  
	  public WebElement getOrgSettingsPage() { return OrgSettingsPage; }
	  
	  public void setOrgSettingsPage(WebElement orgSettingsPage) { OrgSettingsPage
	  = orgSettingsPage; }
	 
	  @FindBy(how = How.XPATH, using = "//select[@id='SELECT_AUTO_MODE']")
	  public WebElement AutoMode;
	
	  
	  
	public WebElement getAutoMode() {
		return AutoMode;
	}

	public void setAutoMode(WebElement autoMode) {
		AutoMode = autoMode;
	}

	@FindBy(how = How.XPATH, using = "//select[@id='SELECT_STATUS_OVERRIDE']")
	public WebElement StatusOverride;
	
	@FindBy(how = How.XPATH, using = "//select[@id='SELECT_MIN_CONSENT_AGE']")
	public WebElement MinAge;
	
	@FindBy(how = How.XPATH, using = "//select[@id='SELECT_STAY_HOME_ACTION_LIST']")
	public WebElement CaseMgmtStayHome;
	
	@FindBy(how = How.XPATH, using = "//select[@id='SELECT_IMMUNIZATION_RECORDS']")
	public WebElement Immunization;
	
	@FindBy(how = How.XPATH, using = "//select[@id='SELECT_ADVANCED_TESTING']")
	public WebElement AdvancedTesting;
	
	@FindBy(how = How.XPATH, using = "//select[@id='SELECT_EMAIL_LOGO']")
	public WebElement EmailLogo;
	
	@FindBy(how = How.XPATH, using = "//select[@id='SELECT_CHECKIN_ENABLED']")
	public WebElement CheckIn;
	
	@FindBy(how = How.XPATH, using = "//select[@id='SELECT_CONTACT_TRACING']")
	public WebElement ContactTracing;
	
	@FindBy(how = How.XPATH, using = "//select[@id='SELECT_DAILY_CHECK_IN']")
	public WebElement DailyCheckin;
	
	
	public WebElement getStatusOverride() {
		return StatusOverride;
	}

	public void setStatusOverride(WebElement statusOverride) {
		StatusOverride = statusOverride;
	}

	public WebElement getMinAge() {
		return MinAge;
	}

	public void setMinAge(WebElement minAge) {
		MinAge = minAge;
	}

	public WebElement getCaseMgmtStayHome() {
		return CaseMgmtStayHome;
	}

	public void setCaseMgmtStayHome(WebElement caseMgmtStayHome) {
		CaseMgmtStayHome = caseMgmtStayHome;
	}

	public WebElement getImmunization() {
		return Immunization;
	}

	public void setImmunization(WebElement immunization) {
		Immunization = immunization;
	}

	public WebElement getAdvancedTesting() {
		return AdvancedTesting;
	}

	public void setAdvancedTesting(WebElement advancedTesting) {
		AdvancedTesting = advancedTesting;
	}

	public WebElement getEmailLogo() {
		return EmailLogo;
	}

	public void setEmailLogo(WebElement emailLogo) {
		EmailLogo = emailLogo;
	}

	public WebElement getCheckIn() {
		return CheckIn;
	}

	public void setCheckIn(WebElement checkIn) {
		CheckIn = checkIn;
	}

	public WebElement getContactTracing() {
		return ContactTracing;
	}

	public void setContactTracing(WebElement contactTracing) {
		ContactTracing = contactTracing;
	}

	public WebElement getDailyCheckin() {
		return DailyCheckin;
	}

	public void setDailyCheckin(WebElement dailyCheckin) {
		DailyCheckin = dailyCheckin;
	}

	@FindBy(how = How.XPATH, using = "//input[@id='appUserTypeLabel0.value']")
	public WebElement StudentSingularLabel;
	
	@FindBy(how = How.XPATH, using = "//input[@id='appUserTypeLabel1.value']")
	public WebElement StudentPluralLabel;
	
	@FindBy(how = How.XPATH, using = "//input[@id='appUserTypeLabel2.value']")
	public WebElement ParentSingularLabel;
	
	@FindBy(how = How.XPATH, using = "//input[@id='appUserTypeLabel3.value']")
	public WebElement ParentPluralLabel;
	
	@FindBy(how = How.XPATH, using = "//input[@id='appUserTypeLabel4.value']")
	public WebElement FacultySingularLabel;
	
	@FindBy(how = How.XPATH, using = "//input[@id='appUserTypeLabel5.value']")
	public WebElement FacultyPluralLabel;
	
	@FindBy(how = How.XPATH, using = "//input[@id='appUserTypeLabel6.value']")
	public WebElement EmployeeSingularLabel;
	
	@FindBy(how = How.XPATH, using = "//input[@id='appUserTypeLabel7.value']")
	public WebElement EmployeePluralLabel;
	
	public WebElement getStudentSingularLabel() {
		return StudentSingularLabel;
	}

	public void setStudentSingularLabel(WebElement studentSingularLabel) {
		StudentSingularLabel = studentSingularLabel;
	}

	public WebElement getStudentPluralLabel() {
		return StudentPluralLabel;
	}

	public void setStudentPluralLabel(WebElement studentPluralLabel) {
		StudentPluralLabel = studentPluralLabel;
	}

	public WebElement getParentSingularLabel() {
		return ParentSingularLabel;
	}

	public void setParentSingularLabel(WebElement parentSingularLabel) {
		ParentSingularLabel = parentSingularLabel;
	}

	public WebElement getParentPluralLabel() {
		return ParentPluralLabel;
	}

	public void setParentPluralLabel(WebElement parentPluralLabel) {
		ParentPluralLabel = parentPluralLabel;
	}

	public WebElement getFacultySingularLabel() {
		return FacultySingularLabel;
	}

	public void setFacultySingularLabel(WebElement facultySingularLabel) {
		FacultySingularLabel = facultySingularLabel;
	}

	public WebElement getFacultyPluralLabel() {
		return FacultyPluralLabel;
	}

	public void setFacultyPluralLabel(WebElement facultyPluralLabel) {
		FacultyPluralLabel = facultyPluralLabel;
	}

	public WebElement getEmployeeSingularLabel() {
		return EmployeeSingularLabel;
	}

	public void setEmployeeSingularLabel(WebElement employeeSingularLabel) {
		EmployeeSingularLabel = employeeSingularLabel;
	}

	public WebElement getEmployeePluralLabel() {
		return EmployeePluralLabel;
	}

	public void setEmployeePluralLabel(WebElement employeePluralLabel) {
		EmployeePluralLabel = employeePluralLabel;
	}
	
	@FindBy(how = How.XPATH, using = "//input[@id='labels0.value']")
	public WebElement LocationSingularLabel;
	
	@FindBy(how = How.XPATH, using = "//input[@id='labels1.value']")
	public WebElement LocationPluralLabel;
	
	@FindBy(how = How.XPATH, using = "//input[@id='labels2.value']")
	public WebElement GroupSingularLabel;

	@FindBy(how = How.XPATH, using = "//input[@id='labels3.value']")
	public WebElement GroupPluralLabel;
	
	@FindBy(how = How.XPATH, using = "//input[@id='labels4.value']")
	public WebElement TagSingularLabel;

	@FindBy(how = How.XPATH, using = "//input[@id='labels5.value']")
	public WebElement TagPluralLabel;
	
	public WebElement getLocationSingularLabel() {
		return LocationSingularLabel;
	}

	public void setLocationSingularLabel(WebElement locationSingularLabel) {
		LocationSingularLabel = locationSingularLabel;
	}

	public WebElement getLocationPluralLabel() {
		return LocationPluralLabel;
	}

	public void setLocationPluralLabel(WebElement locationPluralLabel) {
		LocationPluralLabel = locationPluralLabel;
	}

	public WebElement getGroupSingularLabel() {
		return GroupSingularLabel;
	}

	public void setGroupSingularLabel(WebElement groupSingularLabel) {
		GroupSingularLabel = groupSingularLabel;
	}

	public WebElement getGroupPluralLabel() {
		return GroupPluralLabel;
	}

	public void setGroupPluralLabel(WebElement groupPluralLabel) {
		GroupPluralLabel = groupPluralLabel;
	}

	public WebElement getTagSingularLabel() {
		return TagSingularLabel;
	}

	public void setTagSingularLabel(WebElement tagSingularLabel) {
		TagSingularLabel = tagSingularLabel;
	}

	public WebElement getTagPluralLabel() {
		return TagPluralLabel;
	}

	public void setTagPluralLabel(WebElement tagPluralLabel) {
		TagPluralLabel = tagPluralLabel;
	}

	@FindBy(how = How.XPATH, using = "//input[@id='roleLabel0.value']")
	public WebElement OwnerLabel;
	
	@FindBy(how = How.XPATH, using = "//input[@id='roleLabel1.value']")
	public WebElement AdminLabel;
	
	@FindBy(how = How.XPATH, using = "//input[@id='roleLabel2.value']")
	public WebElement LocationManagerLabel;
	
	@FindBy(how = How.XPATH, using = "//input[@id='roleLabel3.value']")
	public WebElement ManualCheckinLabel;
	
	@FindBy(how = How.XPATH, using = "//input[@id='roleLabel4.value']")
	public WebElement SettingsManagerLabel;
	
	@FindBy(how = How.XPATH, using = "//input[@id='roleLabel5.value']")
	public WebElement UserManagerLabel;
	
	@FindBy(how = How.XPATH, using = "//input[@id='roleLabel6.value']")
	public WebElement VisitorManagerLabel;
	
	@FindBy(how = How.XPATH, using = "//input[@id='roleLabel7.value']")
	public WebElement TestingManagerLabel;
	
	public WebElement getOwnerLabel() {
		return OwnerLabel;
	}

	public void setOwnerLabel(WebElement ownerLabel) {
		OwnerLabel = ownerLabel;
	}

	public WebElement getAdminLabel() {
		return AdminLabel;
	}

	public void setAdminLabel(WebElement adminLabel) {
		AdminLabel = adminLabel;
	}

	public WebElement getLocationManagerLabel() {
		return LocationManagerLabel;
	}

	public void setLocationManagerLabel(WebElement locationManagerLabel) {
		LocationManagerLabel = locationManagerLabel;
	}

	public WebElement getManualCheckinLabel() {
		return ManualCheckinLabel;
	}

	public void setManualCheckinLabel(WebElement manualCheckinLabel) {
		ManualCheckinLabel = manualCheckinLabel;
	}

	public WebElement getSettingsManagerLabel() {
		return SettingsManagerLabel;
	}

	public void setSettingsManagerLabel(WebElement settingsManagerLabel) {
		SettingsManagerLabel = settingsManagerLabel;
	}

	public WebElement getUserManagerLabel() {
		return UserManagerLabel;
	}

	public void setUserManagerLabel(WebElement userManagerLabel) {
		UserManagerLabel = userManagerLabel;
	}

	public WebElement getVisitorManagerLabel() {
		return VisitorManagerLabel;
	}

	public void setVisitorManagerLabel(WebElement visitorManagerLabel) {
		VisitorManagerLabel = visitorManagerLabel;
	}

	public WebElement getTestingManagerLabel() {
		return TestingManagerLabel;
	}

	public void setTestingManagerLabel(WebElement testingManagerLabel) {
		TestingManagerLabel = testingManagerLabel;
	}

	@FindBy(how = How.XPATH, using = "//input[@id='departmentLabels0.value']")
	public WebElement DeptLabelEmployeeSingular;
	
	@FindBy(how = How.XPATH, using = "//input[@id='departmentLabels1.value']")
	public WebElement DeptLabelEmployeePlural;
	
	@FindBy(how = How.XPATH, using = "//input[@id='departmentLabels2.value']")
	public WebElement DeptLabelFacultySingular;
	
	@FindBy(how = How.XPATH, using = "//input[@id='departmentLabels3.value']")
	public WebElement DeptLabelFacultyPlural;
	
	@FindBy(how = How.XPATH, using = "//input[@id='departmentLabels4.value']")
	public WebElement DeptLabelStudentSingular;
	
	@FindBy(how = How.XPATH, using = "//input[@id='departmentLabels5.value']")
	public WebElement DeptLabelStudentPlural;
	
	@FindBy(how = How.XPATH, using = "//input[@id='departmentLabels6.value']")
	public WebElement DeptLabelParentSingular;
	
	@FindBy(how = How.XPATH, using = "//input[@id='departmentLabels7.value']")
	public WebElement DeptLabelParentPlural;
	
	public WebElement getDeptLabelEmployeeSingular() {
		return DeptLabelEmployeeSingular;
	}

	public void setDeptLabelEmployeeSingular(WebElement deptLabelEmployeeSingular) {
		DeptLabelEmployeeSingular = deptLabelEmployeeSingular;
	}

	public WebElement getDeptLabelEmployeePlural() {
		return DeptLabelEmployeePlural;
	}

	public void setDeptLabelEmployeePlural(WebElement deptLabelEmployeePlural) {
		DeptLabelEmployeePlural = deptLabelEmployeePlural;
	}

	public WebElement getDeptLabelFacultySingular() {
		return DeptLabelFacultySingular;
	}

	public void setDeptLabelFacultySingular(WebElement deptLabelFacultySingular) {
		DeptLabelFacultySingular = deptLabelFacultySingular;
	}

	public WebElement getDeptLabelFacultyPlural() {
		return DeptLabelFacultyPlural;
	}

	public void setDeptLabelFacultyPlural(WebElement deptLabelFacultyPlural) {
		DeptLabelFacultyPlural = deptLabelFacultyPlural;
	}

	public WebElement getDeptLabelStudentSingular() {
		return DeptLabelStudentSingular;
	}

	public void setDeptLabelStudentSingular(WebElement deptLabelStudentSingular) {
		DeptLabelStudentSingular = deptLabelStudentSingular;
	}

	public WebElement getDeptLabelStudentPlural() {
		return DeptLabelStudentPlural;
	}

	public void setDeptLabelStudentPlural(WebElement deptLabelStudentPlural) {
		DeptLabelStudentPlural = deptLabelStudentPlural;
	}

	public WebElement getDeptLabelParentSingular() {
		return DeptLabelParentSingular;
	}

	public void setDeptLabelParentSingular(WebElement deptLabelParentSingular) {
		DeptLabelParentSingular = deptLabelParentSingular;
	}

	public WebElement getDeptLabelParentPlural() {
		return DeptLabelParentPlural;
	}

	public void setDeptLabelParentPlural(WebElement deptLabelParentPlural) {
		DeptLabelParentPlural = deptLabelParentPlural;
	}
	
	@FindBy(how = How.XPATH, using = "//button[normalize-space()='Save']")
	public WebElement button_Save;
	

	public WebElement getButton_Save() {
		return button_Save;
	}

	public void setButton_Save(WebElement button_Save) {
		this.button_Save = button_Save;
	}

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Create Organization')]")
	public WebElement button_CreateOrg;

	public WebElement getButton_CreateOrg() {
		return button_CreateOrg;
	}

	public void setButton_CreateOrg(WebElement button_CreateOrg) {
		this.button_CreateOrg = button_CreateOrg;
	}

	@FindBy(how = How.ID, using = "orgName")
	public WebElement input_OrgName;

	public WebElement getInput_OrgName() {
		return input_OrgName;
	}

	public void setInput_OrgName(WebElement input_OrgName) {
		this.input_OrgName = input_OrgName;
	}

	@FindBy(how = How.ID, using = "organizationType")
	public WebElement input_OrgType;

	public WebElement getInput_OrgType() {
		return input_OrgType;
	}

	public void setInput_OrgType(WebElement input_OrgType) {
		this.input_OrgType = input_OrgType;
	}

	@FindBy(how = How.ID, using = "contactName")
	public WebElement input_ContactName;

	public WebElement getInput_ContactName() {
		return input_ContactName;
	}

	public void setInput_ContactName(WebElement input_ContactName) {
		this.input_ContactName = input_ContactName;
	}

	@FindBy(how = How.ID, using = "contactEmail")
	public WebElement input_ContactEmail;

	public WebElement getInput_ContactEmail() {
		return input_ContactEmail;
	}

	public void setInput_ContactEmail(WebElement input_ContactEmail) {
		this.input_ContactEmail = input_ContactEmail;
	}

	@FindBy(how = How.ID, using = "oUsername")
	public WebElement input_LoginEmail;

	public WebElement getInput_LoginEmail() {
		return input_LoginEmail;
	}

	public void setInput_LoginEmail(WebElement input_LoginEmail) {
		this.input_LoginEmail = input_LoginEmail;
	}

	@FindBy(how = How.ID, using = "password")
	public WebElement input_Password;

	public WebElement getInput_Password() {
		return input_Password;
	}

	public void setInput_Password(WebElement input_Password) {
		this.input_Password = input_Password;
	}

	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Submit')]")
	public WebElement button_Submit;

	public WebElement getButton_Submit() {
		return button_Submit;
	}

	public void setButton_Submit(WebElement button_Submit) {
		this.button_Submit = button_Submit;
	}
	
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Updated successfully']")
	public WebElement SuccessMessage;
	
	

	public WebElement getSuccessMessage() {
		return SuccessMessage;
	}

	public void setSuccessMessage(WebElement successMessage) {
		SuccessMessage = successMessage;
	}

	@FindBy(how = How.ID, using = "mobileNumber")
	public WebElement input_Phone;

	public WebElement getInput_Phone() {
		return input_Phone;
	}

	public void setInput_Phone(WebElement input_Phone) {
		this.input_Phone = input_Phone;
	}

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Organization Created SuccessFully')]")
	public WebElement msg_Success;

	public WebElement getMsg_Success() {
		return msg_Success;
	}

	public void setMsg_Success(WebElement msg_Success) {
		this.msg_Success = msg_Success;
	}

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Please Enter a Valid Organization Name')]")
	public WebElement msg_InvalidOrgName;

	public WebElement getMsg_InvalidOrgName() {
		return msg_InvalidOrgName;
	}

	public void setMsg_InvalidOrgName(WebElement msg_InvalidOrgName) {
		this.msg_InvalidOrgName = msg_InvalidOrgName;
	}

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Please Enter a Valid Contact Name')]")
	public WebElement msg_InvalidContactName;

	public WebElement getMsg_InvalidContactName() {
		return msg_InvalidContactName;
	}

	public void setMsg_InvalidContactName(WebElement msg_InvalidContactName) {
		this.msg_InvalidContactName = msg_InvalidContactName;
	}

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Please Enter Valid Mobile Number')]")
	public WebElement msg_InvalidMobile;

	public WebElement getMsg_InvalidMobile() {
		return msg_InvalidMobile;
	}

	public void setMsg_InvalidMobile(WebElement msg_InvalidMobile) {
		this.msg_InvalidMobile = msg_InvalidMobile;
	}

}
