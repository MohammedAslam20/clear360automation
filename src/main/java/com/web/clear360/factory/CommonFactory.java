package com.web.clear360.factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.web.clear360.utility.BaseclassWeb;

public class CommonFactory extends BaseclassWeb {

	// This is a constructor, as every page need a base driver to find web elements
	public CommonFactory(WebDriver driver) {
		super();
	}

	@FindBy(how = How.ID, using = "firstName")
	public WebElement FirstName;

	public WebElement getFirstName() {
		return FirstName;
	}

	public void setFirstName(WebElement firstName) {
		FirstName = firstName;
	}

	@FindBy(how = How.ID, using = "lastName")
	public WebElement LastName;

	public WebElement getLastName() {
		return LastName;
	}

	public void setLastName(WebElement lastName) {
		LastName = lastName;
	}

	@FindBy(how = How.XPATH, using = "//div[@class='logged-user-i logged-user-i1']//span[contains(text(),'Manage Users')]")
	public WebElement menu_ManageUsers;

	public WebElement getMenu_ManageUsers() {
		return menu_ManageUsers;
	}

	public void setMenu_ManageUsers(WebElement menu_ManageUsers) {
		this.menu_ManageUsers = menu_ManageUsers;
	}
	
	@FindBy(how = How.XPATH, using = "//div[@class='logged-user-i logged-user-i1']//span[contains(text(),'Settings')]")
	public WebElement menu_Settings;


	public WebElement getMenu_Settings() {
		return menu_Settings;
	}

	public void setMenu_Settings(WebElement menu_Settings) {
		this.menu_Settings = menu_Settings;
	}
	
	
	@FindBy(how = How.XPATH, using = "//div[@class='logged-user-i logged-user-i1']//span[contains(text(),'Manage Organizations')]")
	public WebElement menu_ManageOrganizations;

	public WebElement getMenu_ManageOrganizations() {
		return menu_ManageOrganizations;
	}

	public void setMenu_ManageOrganizations(WebElement menu_ManageOrganizations) {
		this.menu_ManageOrganizations = menu_ManageOrganizations;
	}

	@FindBy(how = How.XPATH, using = "//div[@class='avatar-w']//i[@class='os-icon os-icon-menu']")
	public WebElement menu_Hover;

	public WebElement getMenu_Hover() {
		return menu_Hover;
	}

	public void setMenu_Hover(WebElement menu_Hover) {
		this.menu_Hover = menu_Hover;
	}

	@FindBy(how = How.XPATH, using = "//div[@class='logged-user-i logged-user-i1']//span[contains(text(),'Check-in')]")
	public WebElement menu_Checkin;

	public WebElement getMenu_Checkin() {
		return menu_Checkin;
	}

	public void setMenu_Checkin(WebElement menu_Checkin) {
		this.menu_Checkin = menu_Checkin;
	}

	@FindBy(how = How.XPATH, using = "//div[@class='logged-user-i logged-user-i1']//span[contains(text(),'Dashboard')]")
	public WebElement menu_Dashboard;

	public WebElement getMenu_Dashboard() {
		return menu_Dashboard;
	}

	public void setMenu_Dashboard(WebElement menu_Dashboard) {
		this.menu_Dashboard = menu_Dashboard;
	}

	@FindBy(how = How.XPATH, using = "//div[@class='logged-user-i logged-user-i1']//span[contains(text(),'Logout')]")
	public WebElement menu_Logout;

	public WebElement getMenu_Logout() {
		return menu_Logout;
	}

	public void setMenu_Logout(WebElement menu_Logout) {
		this.menu_Logout = menu_Logout;
	}

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'You have been logged out.')]")
	public WebElement msg_Logout;

	public WebElement getMsg_Logout() {
		return msg_Logout;
	}

	public void setMsg_Logout(WebElement msg_Logout) {
		this.msg_Logout = msg_Logout;
	}

}
