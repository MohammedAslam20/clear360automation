package com.web.clear360.factory;
  
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.support.FindBy;
	import org.openqa.selenium.support.How;


	import com.web.clear360.utility.BaseclassWeb;

	public class StaffFactory extends BaseclassWeb{

		//This is a constructor, as every page need a base driver to find web elements
		public StaffFactory(WebDriver driver) {
			super();
		}
		

		@FindBy(how = How.XPATH, using = "//button[contains(text(),'Create New Staff')]")     
		public WebElement button_NewStaff ;


		public WebElement getButton_NewStaff() {
			return button_NewStaff;
		}


		public void setButton_NewStaff(WebElement button_NewStaff) {
			this.button_NewStaff = button_NewStaff;
		}
		@FindBy(how = How.XPATH, using = "//input[@id='firstName']")     
		public WebElement FirstName ;


		public WebElement getFirstName() {
			return FirstName;
		}


		public void setFirstName(WebElement firstName) {
			FirstName = firstName;
		}  

		@FindBy(how = How.XPATH, using = "//input[@id='lastName']")     
		public WebElement LastName ;


		public WebElement getLastName() {
			return LastName;
		}


		public void setLastName(WebElement lastName) {
			LastName = lastName;
		}
		@FindBy(how = How.XPATH, using = "//input[@id='mobile']")     
		public WebElement Mobile ;


		public WebElement getMobile() {
			return Mobile;
		}


		public void setMobile(WebElement mobile) {
			Mobile = mobile;
		}
		@FindBy(how = How.XPATH, using = "//input[@id='uniqueId']")     
		public WebElement uniqueId ;


		public WebElement getUniqueId() {
			return uniqueId;
		}


		public void setUniqueId(WebElement uniqueId) {
			this.uniqueId = uniqueId;
		}
		@FindBy(how = How.XPATH, using = "//button[contains(text(),'Submit')]")     
		public WebElement Btn_Submit ;


		public WebElement getBtn_Submit() {
			return Btn_Submit;
		}


		public void setBtn_Submit(WebElement btn_Submit) {
			Btn_Submit = btn_Submit;
		}
		@FindBy(how = How.XPATH, using = "//button[contains(text(),'Create New Faculty')]")     
		public WebElement Btn_NewFaculty;


		public WebElement getBtn_NewFaculty() {
			return Btn_NewFaculty;
		}


		public void setBtn_NewFaculty(WebElement btn_NewFaculty) {
			Btn_NewFaculty = btn_NewFaculty;
		}
		@FindBy(how = How.XPATH, using = "//button[contains(text(),'Create New Guardian')]")     
		public WebElement Btn_NewGuardian;


		public WebElement getBtn_NewGuardian() {
			return Btn_NewGuardian;
		}


		public void setBtn_NewGuardian(WebElement btn_NewGuardian) {
			Btn_NewGuardian = btn_NewGuardian;
		}
		@FindBy(how = How.XPATH, using = "//button[contains(text(),'Create New Student')]")     
		public WebElement Btn_NewStudent;


		public WebElement getBtn_NewStudent() {
			return Btn_NewStudent;
		}


		public void setBtn_NewStudent(WebElement btn_NewStudent) {
			Btn_NewStudent = btn_NewStudent;
		}
		@FindBy(how = How.XPATH, using = "//input[@id='dob']")     
		public WebElement DOB;


		public WebElement getDOB() {
			return DOB;
		}


		public void setDOB(WebElement dOB) {
			DOB = dOB;
		}
		@FindBy(how = How.XPATH, using = "//input[@id='delegate-0']")     
		public WebElement GuardNumber;


		public WebElement getGuardNumber() {
			return GuardNumber;
		}


		public void setGuardNumber(WebElement guardNumber) {
			GuardNumber = guardNumber;
		}
	}
	

		
	
