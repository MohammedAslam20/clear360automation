package com.web.clear360.utility;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.WebDriver;
import com.web.clear360.factory.CheckinFactory;

public class CheckinCommon extends BaseclassWeb {

	public void verifySymptomValue(String actualCheckinValue) {
		String expectedCheckinValue = "Fever";
		try {
			assertEquals(actualCheckinValue, expectedCheckinValue);

		} catch (AssertionError e) {
			System.out.println("Symptom submitted in checkin is NOT matching on health profile");
			throw e;
		}
		System.out.println("Symptom submitted in checkin is matching on health profile");
	}

	public void checkinAllNo(String statusValueTravel, String statusValueExposure, String statusValuePositiveTesting,
			String statusValueNegativetiveTesting) {
		String expected_statusValue = "No";

		try {
			assertEquals(statusValuePositiveTesting, expected_statusValue);
			assertEquals(statusValueTravel, expected_statusValue);
			assertEquals(statusValueExposure, expected_statusValue);
			assertEquals(statusValueNegativetiveTesting, expected_statusValue);

		} catch (AssertionError e) {
			System.out.println("Value NO submitted in checkin is NOT matching on health profile");
			throw e;
		}
		System.out.println("Value NO submitted in checkin is matching on health profile");
	}

	public void AttendanceCheck(String staffAttendanceValue) {
		String expected_staffAttendanceValue = "Online";
		try {
			assertEquals(staffAttendanceValue, expected_staffAttendanceValue);

		} catch (AssertionError e) {
			System.out.println("Attendance value submitted in checkin is NOT matching on manage users");
			throw e;
		}
		System.out.println("Attendance value submitted in checkin is matching on manage users");
	}

	public void checkinValueCheck(String actualCheckinValue) {
		String expectedCheckinValue = "Yes";
		try {
			assertEquals(actualCheckinValue, expectedCheckinValue);

		} catch (AssertionError e) {
			System.out.println("Value YES submitted in checkin is NOT matching on health profile");
			throw e;
		}
		System.out.println("Value YES submitted in checkin is matching on health profile");
	}

	public void close_StatusModal(WebDriver driver, CheckinFactory checkin) throws InterruptedException {

		driver.switchTo().activeElement();
		Thread.sleep(2000);
		checkin.getButton_StatusModalOk().click();
		Thread.sleep(2000);
	}

	public void search_UniqueId(CheckinFactory checkin) {

		checkin.getInput_CheckinSearch().sendKeys("8988");
		checkin.getButton_CheckinSearch().click();
	}

}
