package com.web.clear360.utility;

import static org.testng.Assert.assertEquals;

public class AdminCommon extends BaseclassWeb {

	public void fnameError(String actualError) {
		String expectedError = "Please Enter a Valid First Name.";
		try {
			assertEquals(actualError, expectedError);

		} catch (AssertionError e) {
			System.out.println("Submitted valid first name");
			throw e;
		}
		System.out.println("Submitted invalid first name");
	}

	public void lnameError(String actualError) {
		String expectedError = "Please Enter a Valid Last Name.";
		try {
			assertEquals(actualError, expectedError);

		} catch (AssertionError e) {
			System.out.println("Submitted valid last name");
			throw e;
		}
		System.out.println("Submitted invalid last name");
	}

	public void mobileError(String actualError) {
		String expectedError = "Please Enter Valid Mobile Number";
		try {
			assertEquals(actualError, expectedError);

		} catch (AssertionError e) {
			System.out.println("Submitted valid Mobile number");
			throw e;
		}
		System.out.println("Submitted invalid Mobile number");
	}

	public void emailError(String actualError) {
		String expectedError = "User Email Already Exist";
		try {
			assertEquals(actualError, expectedError);

		} catch (AssertionError e) {
			System.out.println("Submitted valid Email");
			throw e;
		}
		System.out.println("Submitted invalid Email");
	}
}
