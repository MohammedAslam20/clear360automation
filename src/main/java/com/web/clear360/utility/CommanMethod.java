package com.web.clear360.utility;

import static org.testng.Assert.assertEquals;

import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.web.clear360.factory.CommonFactory;
import com.web.clear360.factory.LoginFactory;

public class CommanMethod {

	public static WebDriver driver;
	public String baseUrl = "https://dev.clear360.com/login";

	static String FirstName_Error = "First name is invalid in the CSV file at row : 2";
	static String LastName_Error = "Last name is invalid in the CSV file at row : 3";
	static String DOB_Error = "Date of birth is required in the CSV file at row : 4";
	static String Location_Error = "School/Location is required in the CSV file at row : 5";
	static String Location_Error1 = "School/Location is required in the CSV file at row : 6";
	static String DuplicateMobile_Error = "Duplicate mobile in the CSV file at row : 7";
	static String DuplicateMobile_Error1 = "Duplicate mobile in the CSV file at row : 8";
	static String GuardianName_Error = "Delegate / Guardian is not present in the CSV file at row : 9";
	static String GuardianName_Error1 = "Delegate / Guardian is not present in the CSV file at row : 10";
	static String FirstName_Error1 = "First name is invalid in the CSV file at row : 11";
	static String LastName_Error1 = "Last name is invalid in the CSV file at row : 12";
	static String FirstName_Error2 = "First name is invalid in the CSV file at row : 13";
	static String LastName_Error2 = "Last name is invalid in the CSV file at row : 14";
	static String MobileNumber_Error = "Mobile number is invalid in the CSV file at row";
	static String UniqueId_Error = "Unique ID is invalid in the CSV file at row : 16";
	static String CSVEmpty_Error = "CSV contains empty rows";
	static String Successfully_Error = "1 record(s) added successfully";
	static String Fever = "Fever";
	static String FeverTem = "37 °C";
	static String Cough = "Cough or worsening of chronic cough";
	static String Breath = "Shortness of Breath";
	static String Headache = "Headache";
	static String Fatigue = "Fatigue";
	static String TestedPositive = "I tested positive for COVID-19";
	static String Tavel = "I have traveled to another country since my last check-in";
	static String Exposure = "I have provided care or been in close contact with a person who is feeling unwell since my last check-in";

	static String TavelDate = "Date of return: 04/10/2020";
	static String ExposureDate = "Date of return: 05/10/2020";
	static String TestedPositiveDate = "Date of result: 03/10/2020";
	static String InvalidMobile = "Mobile number is invalid";
	static String LocReq = "School/Location is required";
	static String DepartReq = "Department/Grade is required";
	static String UniqueIdAlready = "Unique ID is already being used by another user.";
	static String mobileNumberAlreadyser = "Mobile number is already being used by another user.";
	static String expectedLoginError = "Invalid username and password.";
	static String successMessage = "Updated Successfully";

	public static void fileUpload(String filenameupload) throws IOException {

		Runtime.getRuntime().exec("F:\\AurigaIT\\Web\\CSV\\FileUploads.exe" + " " + filenameupload);

	}

	public static void errorCheck(WebDriver driver) {

		String ActualText1 = driver
				.findElement(By.xpath("//div[@class='alert alert-warning alert-dismissible fade show']")).getText();

		if (ActualText1.contains(FirstName_Error)) {
			System.out.println("First Name invalid in CSV");
		}

		if (ActualText1.contains(LastName_Error)) {
			System.out.println("Last Name invalid in CSV");
		}

		if (ActualText1.contains(DOB_Error)) {
			System.out.println("Date of birth is required in the CSV");
		}

		if (ActualText1.contains(Location_Error)) {
			System.out.println("School/Location is required in the CSV");
		}

		if (ActualText1.contains(Location_Error1)) {
			System.out.println("School/Location is required in the CSV");
		}

		if (ActualText1.contains(DuplicateMobile_Error)) {
			System.out.println("Duplicate mobile in the CSV");
		}

		if (ActualText1.contains(DuplicateMobile_Error1)) {
			System.out.println("Duplicate mobile in the CSV");
		}

		if (ActualText1.contains(GuardianName_Error)) {
			System.out.println("Delegate / Guardian is not present in the CSV");
		}

		if (ActualText1.contains(GuardianName_Error1)) {
			System.out.println("Delegate / Guardian is not present in the CSV");
		}

		if (ActualText1.contains(FirstName_Error1)) {
			System.out.println("First Name invalid in CSV");
		}

		if (ActualText1.contains(LastName_Error1)) {
			System.out.println("Last Name invalid in CSV");
		}

		if (ActualText1.contains(FirstName_Error2)) {
			System.out.println("First Name invalid in CSV");
		}

		if (ActualText1.contains(LastName_Error2)) {
			System.out.println("Last Name invalid in CSV");
		}

		if (ActualText1.contains(MobileNumber_Error)) {
			System.out.println("Mobile number is invalid in the CSV");
		}

		if (ActualText1.contains(UniqueId_Error)) {
			System.out.println("Unique ID is invalid in the CSV");
		}

		if (ActualText1.contains(CSVEmpty_Error)) {
			System.out.println("CSV contains empty rows");
		}

		if (ActualText1.contains(Successfully_Error)) {
			System.out.println("Added successfully");
		}

	}

	public static void verifyAppCheckIn(WebDriver driver) {

		String WebCheckInVerify = driver.findElement(By.xpath(
				"//body[@class='menu-position-side menu-side-left full-screen with-content-panel']/div[@class='with-side-panel solid-bg-all']/div[@class='layout-w']/div[@class='content-w']/div[@class='all-wrapper content-i']/div[@class='content-box']/div[@class='row']/div[@class='col-sm-4 d-none1 d-lg-block']/div[2]"))
				.getText();

		if (WebCheckInVerify.contains(Cough)) {
			System.out.println("Cough or worsening of chronic cough");
		}

		if (WebCheckInVerify.contains(Fever)) {
			System.out.println(Fever);
		}

		if (WebCheckInVerify.contains(Breath)) {
			System.out.println(Breath);
		}

		if (WebCheckInVerify.contains(Headache)) {
			System.out.println(Headache);
		}

		if (WebCheckInVerify.contains(Fatigue)) {
			System.out.println(Fatigue);
		}

		if (WebCheckInVerify.contains(FeverTem)) {
			System.out.println(FeverTem);
		}

		else {
			System.out.println("Error");
		}
	}

	public static void verifyAppTestedPositive(WebDriver driver) {

		String VerifytestedPositive = driver.findElement(By.xpath(
				"//body[@class='menu-position-side menu-side-left full-screen with-content-panel']/div[@class='with-side-panel solid-bg-all']/div[@class='layout-w']/div[@class='content-w']/div[@class='all-wrapper content-i']/div[@class='content-box']/div[@class='row']/div[@class='col-sm-4 d-none1 d-lg-block']/div[1]"))
				.getText();

		if (VerifytestedPositive.contains(TestedPositive)) {
			System.out.println(TestedPositive);
		}

		if (VerifytestedPositive.contains(TestedPositiveDate)) {
			System.out.println(TestedPositiveDate);
		}

		if (VerifytestedPositive.contains(Tavel)) {
			System.out.println(Tavel);
		}
		if (VerifytestedPositive.contains(TavelDate)) {
			System.out.println(TavelDate);
		}

		if (VerifytestedPositive.contains(Exposure)) {
			System.out.println(Exposure);
		}

		if (VerifytestedPositive.contains(ExposureDate)) {
			System.out.println(ExposureDate);
		}

	}

	public static void checkValid(WebDriver driver) {
		String ValidationMsg = driver
				.findElement(By.xpath("//div[@class='alert alert-warning alert-dismissible fade show']")).getText();

		if (ValidationMsg.contains(InvalidMobile)) {
			System.out.println("Mobile number is invalid");
		}
		if (ValidationMsg.contains(LocReq)) {
			System.out.println("School/Location is required");
		}
		if (ValidationMsg.contains(DepartReq)) {
			System.out.println("Department/Grade is required");

		}
		if (ValidationMsg.contains(DepartReq)) {
			System.out.println("Unique ID is already being used by another user.");

		}
		if (ValidationMsg.contains(DepartReq)) {
			System.out.println("Mobile number is already being used by another user.");
		}
	}

	public static void loginError(WebDriver driver) {
		String actualLoginError = driver
				.findElement(By.xpath("//div[contains(text(),'Invalid username and password.')]")).getText();
		try {
			assertEquals(actualLoginError, expectedLoginError);

		} catch (AssertionError e) {
			System.out.println("Entered valid username and password.");
			throw e;
		}
		System.out.println("Entered Invalid username and password.");
	}

	public void loadurl(WebDriver driver) {
		driver.get(baseUrl);
	}

	public static void successVerify(String actualSuccessMsg) {
		try {
			assertEquals(actualSuccessMsg, successMessage);

		} catch (AssertionError e) {
			System.out.println("Not Updated successfully");
			throw e;
		}
		System.out.println("Updated successfully");
	}

	public void credentials(LoginFactory login) throws InterruptedException {
		login.getInput_UserName().sendKeys("mangesh.darji+m107@aurigait.com");
		login.getInput_Password().sendKeys("Mangesh@123");
		login.getButton_Login().click();

	}
	
	public void credentialSuperAdmin(LoginFactory login) {
		login.getInput_UserName().sendKeys("urmila.kumari@aurigait.com");
		login.getInput_Password().sendKeys("Auriga@123");
		login.getButton_Login().click();
	}

	public void goto_UsersPage(WebDriver driver, CommonFactory common) throws InterruptedException {

		Actions builder = new Actions(driver);
		WebElement hover = common.getMenu_Hover();
		builder.moveToElement(hover).perform();
		WebElement clk_on_MngUsr = common.getMenu_ManageUsers();
		builder.moveToElement(clk_on_MngUsr).click().perform();
		Thread.sleep(2000);

	}
	public void goto_TagsPage(WebDriver driver, CommonFactory common) throws InterruptedException {

		Actions builder = new Actions(driver);
		WebElement hover = common.getMenu_Hover();
		builder.moveToElement(hover).perform();
		WebElement clk_on_Settings = common.getMenu_Settings();
		builder.moveToElement(clk_on_Settings).click().perform();
		Thread.sleep(2000);

	}
	
	public void goto_MessageTempPage(WebDriver driver, CommonFactory common) throws InterruptedException {

		Actions builder = new Actions(driver);
		WebElement hover = common.getMenu_Hover();
		builder.moveToElement(hover).perform();
		WebElement clk_on_Settings = common.getMenu_Settings();
		builder.moveToElement(clk_on_Settings).click().perform();
		Thread.sleep(2000);

	}
	
	public void goto_OrgPage(WebDriver driver, CommonFactory common) throws InterruptedException {

		Actions builder = new Actions(driver);
		WebElement hover = common.getMenu_Hover();
		builder.moveToElement(hover).perform();
		WebElement clk_on_OrgPage = common.getMenu_ManageOrganizations();
		builder.moveToElement(clk_on_OrgPage).click().perform();
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//a[contains(text(),'Create Organization')]")));
		

	}

	public void goto_CheckinPage(WebDriver driver, CommonFactory common) {
		Actions builder = new Actions(driver);
		WebElement hover = common.getMenu_Hover();
		builder.moveToElement(hover).perform();
		WebElement clk_on_chekin = common.getMenu_Checkin();
		builder.moveToElement(clk_on_chekin).click().perform();
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("searchUniqueId")));
	}

	public void goto_DashboardPage(WebDriver driver, CommonFactory common) {
		Actions builder = new Actions(driver);
		WebElement hover = common.getMenu_Hover();
		builder.moveToElement(hover).perform();
		WebElement clk_on_dashboard = common.getMenu_Dashboard();
		builder.moveToElement(clk_on_dashboard).click().perform();
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//body/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/form[1]/div[1]")));
	}

	public void logOut(WebDriver driver, CommonFactory common) {
		Actions builder = new Actions(driver);
		WebElement hover = common.getMenu_Hover();
		builder.moveToElement(hover).perform();
		WebElement logout = common.getMenu_Logout();
		builder.moveToElement(logout).click().perform();
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//div[contains(text(),'You have been logged out.')]")));
		String logoutMsg = common.getMsg_Logout().getText();
		System.out.println(logoutMsg);
		
	}
}
