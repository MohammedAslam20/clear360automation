package com.web.clear360.utility;

import static org.testng.Assert.assertEquals;

public class SuperAdminCommon extends BaseclassWeb {

	public void successVerify(String actualMsg) {
		String expectedMsg = "Organization Created SuccessFully";
		try {
			assertEquals(actualMsg, expectedMsg);

		} catch (AssertionError e) {
			System.out.println("Organization NOT Created");
			throw e;
		}
		System.out.println("Organization Created SuccessFully");
	}
	public void invalidOrgName(String actualMsg) {
		String expectedMsg = "Please Enter a Valid Organization Name";
		try {
			assertEquals(actualMsg, expectedMsg);

		} catch (AssertionError e) {
			System.out.println(" Valid Organization Entered");
			throw e;
		}
		System.out.println("Invalid Organization Entered");
	}
	public void invalidContactName(String actualMsg) {
		String expectedMsg = "Please Enter a Valid Contact Name";
		try {
			assertEquals(actualMsg, expectedMsg);

		} catch (AssertionError e) {
			System.out.println(" Valid Organization Contact Name Entered");
			throw e;
		}
		System.out.println("Invalid Organization Contact Name Entered");
	}
	public void invalidMobile(String actualMsg) {
		String expectedMsg = "Please Enter Valid Mobile Number";
		try {
			assertEquals(actualMsg, expectedMsg);

		} catch (AssertionError e) {
			System.out.println(" Valid Organization Mobile Entered");
			throw e;
		}
		System.out.println("Invalid Organization Mobile Entered");
	}
}
